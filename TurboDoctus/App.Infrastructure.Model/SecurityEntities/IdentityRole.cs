﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace App.Infrastructure.Model.SecurityEntities
{
	public class IdentityRole : IRole<int>
	{
		public IdentityRole()
			: base()
		{

		}

		public IdentityRole(string name)
			: this()
		{
			this.Name = name;
		}


		public int Id
		{
			get
			{
				return RoleId;
			}

		}

		public int RoleId { get; set; }
		public string Name { get; set; }
	}
}
