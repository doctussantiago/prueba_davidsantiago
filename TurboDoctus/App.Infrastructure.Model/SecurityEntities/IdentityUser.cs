﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace App.Infrastructure.Model.SecurityEntities
{
	public class IdentityUser : IUser<int>
	{
		public IdentityUser()
			: base()
		{

		}

		public IdentityUser(string userName)
			: this()
		{
			this.UserName = userName;
		}

		public int Id
		{
			get
			{
				return UserId;
			}

		}
		public int UserId { get; set; }
		public string UserName { get; set; }
		public string FullName { get; set; }
		public virtual string PasswordHash { get; set; }
		public virtual string SecurityStamp { get; set; }
		public bool IsBlocked { get; set; }
	}
}
