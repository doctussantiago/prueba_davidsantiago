//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Infrastructure.Model.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Pelicula
    {
        public int PeliculaId { get; set; }
        public string PeliculaName { get; set; }
        public string Descipcion { get; set; }
        public string ImagenUrl { get; set; }
        public System.DateTime FechaLanzamiento { get; set; }
        public int GeneroId { get; set; }
    
        public virtual Genero Genero { get; set; }
    }
}
