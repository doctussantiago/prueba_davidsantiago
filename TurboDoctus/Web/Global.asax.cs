﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Http;
using System.Web.Routing;
using System.IO;
using App.Web.App_Start;
using App.Web.Areas.Admin;
using App.Common.Classes.Helpers;
using App.Web.Areas.Public;

namespace App.Web
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaHelper.RegisterArea<AdminAreaRegistration>(RouteTable.Routes, null);
            AreaHelper.RegisterArea<PublicAreaRegistration>(RouteTable.Routes, null);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			AutomapperConfig.CreateMaps();
			AutofacConfig.RegisterDependencies();
			SiteMapConfig.RegisterSiteMap();


		}

	}
}
