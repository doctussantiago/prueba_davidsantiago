/// <reference path="ViewModels/Public/ModalAlert.js" />
/// <reference path="Vendor/Doctus/http-client.js" />
/// <reference path="ViewModels/Public/FormViewModel.js" />
/// <reference path="Vendor/Subin/record.js" />
/// <reference path="Vendor/jQuery/jquery-1.11.2.min.js" />


var aceptarSiguiente = null;

$(function () {

    var modalAlert = new ModalAlert();
    var momRecorder = new MomRecorder();
    var site = new Site();
    var actionPost = function () { };
    var isIE = msieversion();


    // ENVIAR MENSAJE DE LA MADRE
    $("#a-siguiente-mensaje").click(function (event) {
        event.preventDefault();

        //Limpiamos el 
        showFormError(false);

        //Obtenemos el mensaje
        var mensaje = $("#text-message").val();


        if (mensaje == "") {
            modalAlert.showError("Falta el mensaje", "Recuerda ingresar el mensaje para mam&aacute;.", "Es obligatorio para continuar");
        } else {
            //INICIAMOS EL PROCESO DE POSTEAR EL MENSAJE
            actionPost = function () {

                eventTraking('click', 'EnviarMensajeAbrazo');

                site.submitMessage(
                    function () {
                        $("#text-message").val("");
                        modalAlert.showSuccess("Tu mensaje ha sido enviado exitosamente", "", "");
                        clearFiles();
                    },
                    function (message) {
                        modalAlert.showError("Ha ocurrido un error", message);
                    });
            }
            site.setMessage($("#text-message").val());
            site.showForm();
        }


    });

    //ENVIAR AUDIO MADRE
    $("#a-siguiente-audio").click(function (event) {
        event.preventDefault();
        showFormError(false);


        if (momRecorder.getBase64() == null) {
            modalAlert.showError("Falta el audio", "Recuerda grabar el mensaje para mam&aacute;.", "Es obligatorio para continuar");
        } else {
            //INICIAMOS EL PROCESO DE POSTEAR EL MENSAJE
            actionPost = function () {

                eventTraking('click', 'EnviarMensajeAudio');
                
                site.submitAudio(
                    momRecorder.getBase64(),
                    function () {
                        $("#text-message").val("");
                        momRecorder.erase();
                        modalAlert.showSuccess("Tu audio ha sido enviado exitosamente", "Nuestro equipo evaluar&aacute; la calidad de tu mensaje.", "Recibir&aacute;s una notificaci&oacute;n cuando sea publicado.");
                        clearFiles();
                    },
                    function (message) {
                        modalAlert.showError("Ha ocurrido un error", message);
                    });
            }
            site.setMessage($("#text-message").val());
            site.showForm();
        }
    });
    $("#fb-share").click(function (event) {
        event.preventDefault();
        var url = window.location.href;
        FB.ui({
            method: 'share',
            href: url,
            message: 'En un d�a tan especial puedes hacer feliz a mam� de una manera muy f�cil, s ingresa y elige una de estas dos sorpresas, primera: grabar un mensaje de voz para que mam� lo pueda escuchar en la radio, segunda: contar una historia para que puedas viajar donde mam� y darle un gran abrazo. �con tu �xito sorprende a mam�!'
        }, function (response) { });
    });

    // Si es un dispositivo
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $("#ctn-mobil-gabadora").show();
        $("#ctn-gabadora").hide();
        $("#a-siguiente-audio").hide();
    }


    //FUNCIONES
    function msieversion() {

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
            return true;
        else{
            if (!!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/)) {
                return true;
            } else {
                return false;
            }
        }                 // If another browser, return 0
        

    }

    if (!isIE) {
        $(".player").show();
    }

    //CARGANDO
    $(document).ajaxStart(
        function () {
            $.blockUI({
                baseZ: 2000,
                message: "cargando...",
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }).ajaxStop(
        function () {
            $.unblockUI();
        });

    //POPUPS
    $('.open-popup-link').magnificPopup({
        type: 'inline',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });

    //LOGIN FACEBOOK
    $("#a-loginFB").click(function (event) {
        event.preventDefault();
        showFormError(false);

        //Success(Data:FacebookResponse),Fail(Message)
        site.loginFB(
            function () {
                $.magnificPopup.instance.close();
                site.showForm();
            },
            function (message) {

                modalAlert.showError("Ha ocurrido un error", message);
            }
        );
    });

    //CONTROLES SONIDO
    var timeOutAudio, intervalAudio;
    $("#btn-record").click(function (event) {
        event.preventDefault();

        modalAlert.showError("Aceptar Permisos", "Recuerda aceptar los permisos del micr&#243;fono para poder enviar tu mensaje.", "<a id='a-continue-record' class='sprite-btn-siguiente' href='javascript:aceptarSiguiente();'></a>");

    });

    aceptarSiguiente = function () {
        $.magnificPopup.instance.close();

        momRecorder.record(function () {

            clearInterval(intervalAudio);
            clearTimeout(timeOutAudio);
            var counter = 0;


            timeOutAudio = setTimeout(function () {
                $("#btn-stop").trigger("click");
            }, 11000);
            intervalAudio = setInterval(function () {
                $("#div-container-record").addClass("div-timer-audio");
                var points;
                switch (counter) {
                    case 0: case 3: case 6: case 9:
                        $("#btn-record").addClass("grabar-activo");
                        points = "...";
                        break;
                    case 1: case 4: case 7: case 10:
                        $("#btn-record").removeClass("grabar-activo");
                        points = "..&nbsp;";
                        break;
                    case 2: case 5: case 8: case 11:
                        $("#btn-record").addClass("grabar-activo");
                        points = ".&nbsp;&nbsp;";
                        break;
                }
                $(".timer-audio").html("Grabando" + points + " " + counter + " seg. (M&aacute;x. 10 segundos)");
                counter++;
            }, 1000);
        }, function () { //NO MIC
            modalAlert.showError("Permisos del Micr&#243;fono", "No le has dado permisos al micr&#243;fono", "Revisa que si tengas permisos o refresca la p&#225;gina y cuando te pregunte por los permisos debes aceptarlos");
        }, function () { //NO PERMISOS
            modalAlert.showError("Micr&#243;fono no encontrado", "Tu computador no tiene micr&#243;fono", "Para realizar un audio necesitas un micr&#243;fono");
        });
    }

    $("#btn-stop").click(function (event) {
        event.preventDefault();
        momRecorder.stop();
        clearInterval(intervalAudio);
        clearTimeout(timeOutAudio);
        $(".timer-audio").html("");
        $("#div-container-record").removeClass("div-timer-audio");
        //Removemos la clase
        $("#btn-record").removeClass("grabar-activo");
    });
    $("#btn-borrar").click(function (event) {
        event.preventDefault();
        momRecorder.erase();
        $(".timer-audio").html("M&aacute;x. 10 segundos");
    });

    //FORMUALRIO
    function showFormError(show, text) {
        if (show) {
            $(".ctn-error-m").text(text);
            $(".ctn-error-m").show();
        } else {
            $(".ctn-error-m").hide();
        }
    }
    $("#a-enviar-form").click(function (event) {
        event.preventDefault();
        showFormError(false);
        site.submitForm(
            function () {
                actionPost();
            },
            function (message) {
                showFormError(true, message);
            }
            );
    });



    //CLASES
    function Site() {
        var self = this;

        var hasUserInfo = false; //Si ya posteo no se le pide
        var httpClient = new HttpClient();
        var fb_user = null;
        var message = null;
        var formVM = new FormViewModel();
        formVM.applyBindings();


        self.loginFB = function (successCallback, failCallback) {
            FB.login(function (response) {
                if (response.status === 'connected') {
                    FB.api('/me', function (response) {
                        fb_user = response;
                        self.mapDTO();
                        successCallback();
                    });
                } else if (response.status === 'not_authorized') {
                    failCallback("Para continuar debes autorizar la aplicaci&#243;n en Facebook");
                } else {
                    //failCallback("Ha ocurrido un error tratando de conectarse con Facebook");
                }
            }, { scope: 'public_profile,email' });
        }

        self.userExist = function (mail, yesCallbak, noCallback, errorCallback) {
            httpClient.callService({
                url: "/api/site/UserExist?mail=" + mail,
                onComplete: function (response) {
                    if (response.Header.Success) {
                        if (response.Data) {
                            yesCallbak();
                        } else {
                            noCallback();
                        }
                    } else {
                        errorCallback(response.Header.Success[0]);
                    }
                }
            });
        }

        self.showForm = function () {
            if (hasUserInfo) {
                actionPost();
            } else {
                $("#a-ctn-formulario").trigger("click");
            }
        }

        self.setMessage = function (text) {
            message = text;
        }

        self.submitForm = function (successCallback, failCallback) {
            formVM.save(successCallback, failCallback);
        }

        self.submitMessage = function (successCallback, failCallback) {

            var data = {
                Mail: formVM.item.Mail,
                Message: message
            }

            httpClient.callService({
                url: "/api/site/createmessage",
                data: data,
                onComplete: function (response) {
                    if (response.Header.Success) {
                        successCallback();
                    } else {
                        failCallback(response.Header.Messages[0]);
                    }
                }
            });

        }

        self.submitAudio = function (base64, successCallback, failCallback) {

            var data = {
                Mail: formVM.item.Mail,
                Base64: base64
            }

            httpClient.callService({
                url: "/api/site/createaudio",
                data: data,
                onComplete: function (response) {
                    if (response.Header.Success) {
                        successCallback();
                    } else {
                        failCallback(response.Header.Messages[0]);
                    }
                }
            });

        }


        self.mapDTO = function () {

            var dto = {
                UserId: 0,
                FullName: fb_user.name,
                Mail: fb_user.email,
                PhotoUrl: "https://graph.facebook.com/" + fb_user.id + "/picture?width=200&height=200",
                AceptTermsAndCotidion: true,
                BirthDay: fb_user.birthday,
                Gender: fb_user.gender,
                FBId: fb_user.id,
                FBImageUrl: "https://graph.facebook.com/" + fb_user.id + "/picture?width=200&height=200"
            };

            formVM.map(dto);
        }

    }
    function MomRecorder() {

        var self = this;
        var base64 = null;


        self.record = function (callback, noPermisionCallback, noMicCallbak) {

            self.erase();

            if (isIE) {

                flashVoice.record(callback, noPermisionCallback, noMicCallbak);

            } else {

                $.voice.record(
                    false,
                    function () {
                        callback();
                    },
                noPermisionCallback,
                noMicCallbak);
            }
        }

        self.stop = function () {

            if (isIE) {
                flashVoice.stop(function (data) {
                    base64 = data;
                    $("#audio").attr("src", data);
                    $("#audio")[0].play();
                });



            } else {
                $.voice.export(function (url) {
                    base64 = url;
                    $("#audio").attr("src", url);
                    $("#audio")[0].play();
                    $.voice.stop();
                }, "base64");
            }

        }

        self.erase = function () {
            $("#audio").attr("src", null);
            base64 = null;
        }

        self.getBase64 = function () {
            return base64;
        }

    }
});


var flashVoice = new FlashVoice();
var barTop = false;
var currentinfoBar = "";
var barFooter = false;

function FlashVoice() {
    var self = this;
    self.exportCallbackIn = null;

    self.recordCallbackIn = null;
    self.noPermisionCallbackIn = null;
    self.noMicCallbakIn = null;

    self.record = function (callback, noPermisionCallback, noMicCallbak) {
        self.recordCallbackIn = callback;
        self.noPermisionCallbackIn = noPermisionCallback;
        self.noMicCallbakIn = noMicCallbak;
        flashMovie("FlashMic").record()
    };

    self.stop = function (callback) {
        self.exportCallbackIn = callback;
        flashMovie("FlashMic").stopRecording();

    };

    self.recordCallback = function () {
        self.recordCallbackIn();
    }

    self.recordYesPermisionCallback = function () {

    }

    self.recordNoPermisionCallback = function () {
        self.noPermisionCallbackIn();
    };

    self.exportCallback = function (data) {
        self.exportCallbackIn(data);
    }

    self.noMicCallback = function () {
        var ma = new ModalAlert();
        ma.showError("Permisos del Micr&#243;fono", "No le has dado permisos al micr&#243;fono", "Revisa que si tengas permisos o refresca la pagina y cuando te pregunte por los permisos debes aceptarlos");
    }


}

function openBarTop(info) {

    if (info == "compartir") {
        $(".ctn-siguenos").hide();
        $(".ctn-compartir").show();
    }

    if (info == "siguenos") {
        $(".ctn-compartir").hide();
        $(".ctn-siguenos").show();

    }

    if (!barTop) {

        $(".ctn-networks-top").animate({
            height: "100px"
        }, 500);


        currentinfoBar = info;
        barTop = true;

    } else {

        if (currentinfoBar == info || info == "close") {

            $(".ctn-networks-top").animate({
                height: "0"
            }, 500, function () {
                // Animation complete.
            });

            currentinfoBar = "";
            barTop = false;
        }
    }


}

function openInfoFooter() {

    if (!barFooter) {

        $(".ctn-footer").css("z-index", "2");

        $(".ctn-footer").animate({
            bottom: "0"
        }, 500);

        barFooter = true;

    } else {

        $(".ctn-footer").animate({
            bottom: "-83"
        }, 500, function () {
            $(".ctn-footer").css("z-index", "1");
        });

        barFooter = false;

    }

}

function sendToJS(value) {
    alert(value);
}

function openAllow() {

    $(".ctn-obj-flash").css("z-index", "1");
    $(".ctn-grabadora").css("z-index", "0");
}

function closeAllow() {

    $(".ctn-obj-flash").css("z-index", "0");
    $(".ctn-grabadora").css("z-index", "1");

}

function clearFiles() {

    $('.ctn-form form input').val("");
    
}

function ValidateFormat(formats, format) {
	format = format.replace('.', '');
	var valid = true;

	var index = formats.indexOf(format);

	if (index == -1) {
		valid = false
	}

	return valid;
}