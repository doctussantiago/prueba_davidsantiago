function KendoGridWrapper(config) {
	
	var httpClient = new HttpClient();
	var messageViewModel = new MessageViewModel();


	var self = this;
	  
	//configuraciones de la tabla
	self.gridOptions = {
		//propiedades para el manejo de datos
		data: ko.observableArray(),
		total: ko.observable(),

		//prorpiedades de la tabla
		scrollable: false,
		sortable: false,
		filterable: false,
		resizable: true,
		pageable: false,
		pageSize: 10,
		columns: [],
		tooltipTemplate: [
			'<div class="btn-group-vertical" role="group" aria-label="...">',
				'<a type="button" class="btn btn-default" data-bind="attr: { href: GetEditUrl }" >Editar</a>',
				'<button type="button" class="btn btn-default" data-bind="click: Delete">Eliminar</button>',
			'</div>'
		].join(" "),

		//guarda el widget generado por kendo-knockout
		widget: ko.observable(),
	};
		
	self.gridOptions = $.extend({}, self.gridOptions, config);

	self.gridOptions.template = kendo.template(self.gridOptions.tooltipTemplate);


	//Datasource para el paginador y la tabla
	var dataSource = new kendo.data.DataSource({
		data: self.gridOptions.data,
		pageSize: self.gridOptions.pageSize,
		schema: {
			total: function(response) {
				return self.gridOptions.total();
			}
		},
	});

	//configuraciones del paginador
	self.pagerOptions = {
		data: dataSource,
		change: function(pagerData) {
			if (pagerData.index > 0) {
				self.loadPageData(pagerData.index);
			}
		},
		messages: {
			display: ' {0} - {1} de {2} registros',
			empty: 'No hay registros'
		}
	};

	//carga los datos de la pagina recibida como parametro
	self.loadPageData = function(pageNumber){

		//se setea la pagina en el datasource para que cambie en el paginado
		dataSource.page(pageNumber);

		httpClient.callService("post", config.readUrl + "?page=" + pageNumber, null, function (response) {

			self.gridOptions.data(response.Data.List);
			self.gridOptions.total(response.Data.Count);
		});
	}

	self.removeItemById = function(id){

		//deteminar si dos ids son el mismo
		function ifIdsAreSame(item){ 
			
			return id == config.getDataItemId(item);
		}

		self.gridOptions.data.remove(ifIdsAreSame);
	}

		
	//se aplican los bindings
	ko.applyBindings(self, $("#form-container")[0]);
	ko.applyBindings(messageViewModel, $("#messages-container")[0]);

	//funcionalidad del tooltip
	self.gridOptions.widget().tbody.kendoTooltip({
		show: function (e) {

			//se optiene el "DataItem" asociado a la fila del tooltip
			var currentRow = this.target().closest("tr");
			var kendoGrid = self.gridOptions.widget();
			var dataItem = kendoGrid.dataItem(currentRow);
			var generatedTemplate = self.gridOptions.template(dataItem);
			this.content.html(generatedTemplate);

			//se crea un viewModel que "atienda" las interacciones del usuario con el tooltip
			var tooltipViewModel = ko.mapping.fromJS(dataItem);

			tooltipViewModel.GetEditUrl = ko.computed(function(){
				return  self.gridOptions.editUrl + self.gridOptions.getDataItemId(dataItem);
			});

			tooltipViewModel.Delete = function(nodeViewModel){
				
				if(confirm("Realmente desea eliminar éste registro")){
					
					var dataItemId = config.getDataItemId(ko.mapping.toJS(nodeViewModel));

					//self.removeItemById(dataItemId);

					//se borra de la base de datos
					httpClient.callService({
						url: config.deleteUrl + dataItemId,
						onComplete: function(response){

							messageViewModel.Success(response.Header.Success);
							messageViewModel.Messages(response.Header.Messages);

							//se cargan los datos de la primera pagina si el borrado fue exitoso
							if(response.Header.Success){
								self.loadPageData(1);
							}
						},
					});

				}
			};

			ko.cleanNode(this.content[0]);
			ko.applyBindings(tooltipViewModel, this.content[0]);
		},
		filter: ".actions-tooltip",
		position: "left",
		content: kendo.template(self.gridOptions.tooltipTemplate)
	});

	//se inicializa la grilla con los datos de la primera pagina
	self.loadPageData(1);
		
};