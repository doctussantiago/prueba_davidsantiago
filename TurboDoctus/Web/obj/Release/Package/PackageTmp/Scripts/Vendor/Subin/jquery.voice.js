(function ($) {
    $.extend({
        voice: {
            workerPath: "scripts/vendor/Subin/recorderWorker.js",
            initCalled: false,
            stream: false,
            init: function () {
                try {
                    window.AudioContext = window.AudioContext || window.webkitAudioContext; navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia; window.URL = window.URL || window.webkitURL; if (navigator.getUserMedia === false) { alert('getUserMedia() is not supported in your browser'); }
                    $.voice.context = new AudioContext();
                }
                catch (e) {
                    alert('Este browser no soporta grabaciones de audio (Microfono)');
                }
            },
            record: function (output, callback,noPermisionCallback,noMicCallback) {

                if ($.voice.initCalled === false) {
                    this.init();
                    $.voice.initCalled = true;
                }
                navigator.getUserMedia(
                    { audio: true },
                    function (stream) {
                        var input = $.voice.context.createMediaStreamSource(stream);
                        if (output === true) {
                            input.connect($.voice.context.destination);
                        }
                        $.voice.recorder = new Recorder(input,{ workerPath: $.voice.workerPath });
                        $.voice.stream = stream;
                        $.voice.recorder.record();
                        callback(stream);
                    },
                    function (Ex) {
                        if (Ex.name == "PermissionDeniedError") {
                            noPermisionCallback();
                        } else {
                            noMicCallback();
                        }
                    });
            },

            stop: function () {
                $.voice.recorder.stop();
                $.voice.recorder.clear();
                $.voice.stream.stop();
                return $.voice;
            },
            export: function (callback, type) {
                $.voice.recorder.exportWAV(
                    function (blob) {
                        if (type == "" || type == "blob") {
                            callback(blob);
                        } else if (type == "base64") {
                            var reader = new window.FileReader();
                            reader.readAsDataURL(blob);
                            reader.onloadend = function () {
                                base64data = reader.result;
                                callback(base64data);
                            }
                        } else if (type == "URL") {
                            var url = URL.createObjectURL(blob);
                            callback(url);
                        }
                    });
            }
        }
    });
})(jQuery);