function HttpClient() {

	var self = this;

	self.callService = function (method, url, data, successCallback, failCallback) {

		var options;

		//se recibieron callbacks?
		if (typeof method === "string") {

			options = {
				method: method,
				url: url,
				data: data,
				onSuccess: successCallback,
				onFail: failCallback
			};


		} else {

			options = method;
		}

		var config = {
			method: "post",
			url: url,
			data: data,
			success: function () { },
			onSuccess: function () { },
			fail: function () { },
			onFail: function () { },
			onComplete: function () { },
			complete: function (request) {
				if (request && request.responseJSON) {

					var response = request.responseJSON;
					
					config.onComplete(response);

					if (response.Header.Success) {
						config.onSuccess(response);
					} else {
						config.onFail(response);
					}
				}
			}
		};

		config = $.extend({}, config, options);

		$.ajax(config);
	}

};