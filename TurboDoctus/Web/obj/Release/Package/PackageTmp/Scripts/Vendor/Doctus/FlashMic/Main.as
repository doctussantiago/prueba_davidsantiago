﻿package 
{
	
	import fl.motion.AdjustColor;
	import fl.motion.Source;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.SampleDataEvent;
	import flash.events.StatusEvent;
	import flash.media.Microphone;
	import flash.media.Sound;
	import flash.system.Security;
	import flash.utils.ByteArray;
	import org.bytearray.micrecorder.*;
	import org.bytearray.micrecorder.events.RecordingEvent;
	import org.bytearray.micrecorder.encoder.WaveEncoder;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.events.ActivityEvent;
	import fl.transitions.Tween;
	import fl.transitions.easing.Strong;
	import flash.net.FileReference;
	import flash.external.ExternalInterface;
	import flash.utils.*;
	


	public class Main extends flash.display.MovieClip
	{
		const DELAY_LENGTH:int = 4000; 
		private var mic:Microphone;
		private var permisitionsAsked:Boolean = false;
		private var micHasPermision:Boolean = false;
		private var waveEncoder:WaveEncoder = new WaveEncoder(); //Will encode the data captured by the microphone, part of MicRecorder
		private var recorder:MicRecorder = new MicRecorder(waveEncoder); //Creates a MicRecorder instance and uses the WaveEncoder class to encode
		private var fileReference:FileReference = new FileReference();
		var base64:String = null;
		
		public var btnS:SimpleButton;
		
		
		public function Main():void
		{
			pedirPermisos();
			//Security.showSettings("privacy");
			
			btnS.addEventListener(MouseEvent.CLICK, onSiguiente);
		}

		
		function onSiguiente(event:Object){
			Security.showSettings("privacy");
		}
		
		function pedirPermisos(){
			
			
			mic = Microphone.getMicrophone();
			if(mic){
			mic.gain = 60; 
			mic.rate = 11; 
			mic.setUseEchoSuppression(true); 
			mic.setSilenceLevel(5, 1000); 
			
			
			mic.setLoopBack(true); 
			var intervalId:uint = setTimeout(function(){
				mic.setLoopBack(false); 
			}, 100);
			
			
			mic.addEventListener(StatusEvent.STATUS, this.onMicStatus); 
			recorder.addEventListener(Event.COMPLETE, recordComplete);
			
			
			//MOSTRAMOS LOS PERMISOS			
			callbacks();
			}else {
				
			}
			
			
		}
		
		
		function stopEvent(event){
			stop();
		}
		
		function callbacks(){
			
			ExternalInterface.addCallback("record", record);
			ExternalInterface.addCallback("stopRecording", stopRecording);
			ExternalInterface.addCallback("export",export);
			
		}
		
		
		function record(){
			
			if(mic){}else{
				ExternalInterface.call("flashVoice.noMicCallback");
				return;
			}
			
			if(permisitionsAsked){
				if (micHasPermision) {
					recorder.record();
					ExternalInterface.call("console.log","record callback 1");
					ExternalInterface.call("flashVoice.recordCallback");
				}else{
					ExternalInterface.call("flashVoice.recordNoPermisionCallback");
				}
			}else{
				
				ExternalInterface.call("openAllow");
			}
		}
		
		function stopRecording(){
			recorder.stop();
		}
		
		function clear(){
			
			base64 = null;
		}
		
		function export() {
			if(base64){
			
				ExternalInterface.call("flashVoice.exportCallback", base64);
			}
		}
		
		
		
		
		var soundBytes:ByteArray = new ByteArray();
		function micSampleDataHandler(event:SampleDataEvent):void 
		{ 
			
			while(event.data.bytesAvailable) 
			{ 
				var sample:Number = event.data.readFloat(); 
				soundBytes.writeFloat(sample); 
			} 
		} 
		
		
		function onMicStatus(event:StatusEvent):void 
		{ 
			permisitionsAsked = true;
			
			
			
			if (event.code == "Microphone.Unmuted") 
			{ 
				micHasPermision = true;
				ExternalInterface.call("consolo.log","record callback 2");
				ExternalInterface.call("flashVoice.recordCallback");
				ExternalInterface.call("flashVoice.recordYesPermisionCallback");
				ExternalInterface.call("closeAllow");
				record();
			}  
			else if (event.code == "Microphone.Muted") 
			{ 
				micHasPermision = false;
				ExternalInterface.call("flashVoice.recordNoPermisionCallback");
				
			} 
		}
		
		private function recordComplete(e:Event):void
		{
			base64 = "data:audio/wav;base64," + Base64.encode(recorder.output);
			ExternalInterface.call("flashVoice.exportCallback", base64);
		}
		
		
		
	}
}