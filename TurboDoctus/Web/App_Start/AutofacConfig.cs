﻿using App.Common.Security;
using Autofac;
using Autofac.Integration.Mvc;
using System.Reflection;
using System.Web.Mvc;

namespace App.Web.App_Start
{
	public class AutofacConfig
	{

		public static void RegisterDependencies()
		{
			var builder = new ContainerBuilder();

			builder.RegisterControllers(Assembly.GetExecutingAssembly());
			builder.RegisterType<UserIdentitySession>().As<IUserSession>();
			App.Domain.Config.AutofacConfig.RegisterTypes(builder);

			var container = builder.Build();

			DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
		}
	}
}