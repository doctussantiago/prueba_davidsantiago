﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kendo.Mvc;

namespace App.Web.App_Start
{
	public class SiteMapConfig
	{

		public static void RegisterSiteMap()
		{
			if (!SiteMapManager.SiteMaps.ContainsKey("app"))
			{
				SiteMapManager.SiteMaps.Register<XmlSiteMap>("app", sitemap => sitemap.LoadFrom("~/app.sitemap"));
			}
		}

	}
}