﻿namespace App.Web.App_Start
{
	public class AutomapperConfig
	{

		public static void CreateMaps()
		{
			App.Domain.Config.AutomapperConfig.CreateMaps();
		}

	}
}