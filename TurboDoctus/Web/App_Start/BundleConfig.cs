﻿using System.Web;
using System.Web.Optimization;

namespace App.Web
{
	public class BundleConfig
	{
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/admin").Include(
						"~/Scripts/Vendor/jquery/jquery-1.10.2*",
						"~/Scripts/Vendor/jquery/jquery.validate*",
						"~/Scripts/Vendor/jquery/jquery.validate.unobtrusive*",
						"~/Scripts/Vendor/Bootstrap/bootstrap.min.js",
						"~/Scripts/Vendor/Kendo/kendo.all.min.js",
						"~/Scripts/Vendor/Kendo/kendo.aspnetmvc.min.js",
						"~/Scripts/Vendor/Kendo/kendo.timezones.min.js",
						"~/Scripts/Vendor/Kendo/kendo.timezones.min.js",
						"~/Scripts/Vendor/Kendo/cultures/kendo.culture.es-CO.min.js",
						"~/Scripts/Areas/Admin/main.js"
						));


			/****
			 * LOS BUNDLES DE CSS
			 **/
			bundles.Add(new StyleBundle("~/Content/Bootstrap").Include(
				"~/Content/Vendor/Bootstrap/css/bootstrap.min.css",
				"~/Content/Vendor/Font-Awesome/css/font-awesome.min.css"));

			bundles.Add(new StyleBundle("~/Content/Vendor/Kendo/web/kendocss").Include(
						"~/Content/Vendor/Kendo/web/kendo.common-bootstrap.min.css",
						"~/Content/Vendor/Kendo/web/kendo.bootstrap.min.css",
						"~/Content/Vendor/Kendo/web/kendo.dataviz.min.css",
						"~/Content/Vendor/Kendo/web/kendo.dataviz.bootstrap.min.css"
						));

			bundles.Add(new StyleBundle("~/Content/admin").Include(
						"~/Content/Styles/Admin/Admin.css"
						));
		}
	}
}
