﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using App.Web.Models;
using App.Common.Security;
using System.Security.Principal;

namespace App.Web
{
	public class UserIdentitySession : IUserSession
	{
		public UserIdentitySession()
		{
		}

		public string UserName
		{
			get
			{
				if (HttpContext.Current.User.Identity.IsAuthenticated)
				{
					return HttpContext.Current.User.Identity.Name;
				}

				return string.Empty;
			}
		}

		public string IP
		{
			get
			{
				return App.Common.Helpers.HttpHelper.GetIpAddress();
			}
		}
	}


}
