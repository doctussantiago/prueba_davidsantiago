﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace App.Web.Helpers
{
	public static class HtmlExtensioncs
	{
		public static HtmlString SuccessMessage(this HtmlHelper helper)
		{
			if (!string.IsNullOrEmpty(helper.ViewBag.SuccessMessage))
			{
				StringBuilder builder = new StringBuilder();
				builder.Append("<div class='success-message'>");
				builder.Append(helper.ViewBag.SuccessMessage);
				builder.Append("</div>");
				return new HtmlString(builder.ToString());
			}

			return new HtmlString(string.Empty);
		}

		public static string GetCurrentController(this System.Web.Mvc.HtmlHelper htmlHelper)
		{
			return HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString();
		}
	}
}