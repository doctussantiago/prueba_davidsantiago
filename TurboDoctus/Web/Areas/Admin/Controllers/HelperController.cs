﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Common.Classes.Helpers;

namespace App.Web.Areas.Admin.Controllers
{
	//[Authorize]
	public class HelperController : Controller
	{
		// GET: Admin/Helper
		[HttpPost]
		public ActionResult SaveFile(string fieldName, string path)
		{
			var savedFileName = FileHelper.SaveFile(HttpContext, fieldName, path);
			var data = new { name = savedFileName };
			return Json(data);
		}

		public ActionResult RenderUpload()
		{
			return PartialView("PartialUploadDocument");
		}


	}
}