﻿using System;
using System.Web;
using System.Web.Mvc;
using App.Common.Classes.Extensions;
using App.Common.Localization;
using App.Domain.Base;
using doct.Exception;
using doct.Localization;
using Kendo.Mvc.UI;

namespace App.Web.Areas.Admin.Controllers
{
	public abstract class BaseController<TDTO> : Controller
		where TDTO : class, new()
	{
		private ILocalizationManager _localizator;

		public ILocalizationManager Localizator
		{
			get
			{
				return _localizator ?? (_localizator = new LocalizationManager());
			}

		}

		private IBaseService<TDTO> _service;

		public BaseController(IBaseService<TDTO> service)
		{
			this._service = service;
		}

		public BaseController() { }



		#region Create

		public virtual ActionResult Create(int id = 0)
		{
			var model = GetNewModel(id);

			SetModelProperties(model);

			return View(model);
		}

		[HttpPost]
		[ValidateInput(false)]
		[ValidateAntiForgeryToken]
		public virtual ActionResult Create(TDTO model, int id = 0)
		{
			try
			{
				ServiceCreate(model);
				TempData["SuccessMessage"] = Localizator.GetStringResourceWithFormat("Admin", "Insert_Success");
				return RedirectToAction("Index");
			}
			catch (ValidationRepositoryException e)
			{
				ViewBag.ErrorMessage = e.ToUlHtmlString();
			}
			catch (ApplicationException e)
			{
				ViewBag.ErrorMessage = e.ToUlHtmlString();
			}
			catch (GeneralException ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (Exception ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}

			return View(model);
		}

		protected virtual void ServiceCreate(TDTO model)
		{
			_service.Create(model);
		}

		#endregion

		#region Read

		public virtual ActionResult Index(int id = 0)
		{
			ViewBag.SuccessMessage = TempData["SuccessMessage"];
			return View();
		}

		public virtual ActionResult GetAll([DataSourceRequest]DataSourceRequest request)
		{
			return Json(_service.ExecuteKendoResult(request), JsonRequestBehavior.AllowGet);
		}


		public ActionResult GetDataToDropDown()
		{
			return Json(_service.GetAll(), JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region Update

		public virtual ActionResult Edit(int id = 0)
		{
			var model = GetModelBy(id);

			if (model == null)
			{
				throw new HttpException(404, "Not Found");
			}

			SetModelProperties(model);

			return View(model);
		}

		[HttpPost]
		[ValidateInput(false)]
		[ValidateAntiForgeryToken]
		public virtual ActionResult Edit(int id, TDTO model)
		{
			try
			{
				ServiceUpdate(id, model);
				TempData["SuccessMessage"] = Localizator.GetStringResourceWithFormat("Admin", "Update_Success");
				return RedirectToAction("Index");
			}
			catch (ValidationRepositoryException ex)
			{
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (GeneralException ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (ApplicationException e)
			{
				ViewBag.ErrorMessage = e.ToUlHtmlString();
			}
			catch (Exception ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}

			SetModelProperties(model);

			return View(model);
		}

		/// <summary>
		/// Debe ser sobreescrito si el controlador requiere la funcionalidad de consulta
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		protected virtual TDTO GetModelBy(object id)
		{
			return _service.FindById(id);
		}
		/// <summary>
		/// Debe ser sobreescrito si el controlador requiere la funcionalidad de actualizacion
		/// </summary>
		/// <param name="id"></param>
		/// <param name="model"></param>
		/// <returns></returns>
		protected virtual void ServiceUpdate(int id, TDTO model)
		{
			_service.Update(id, model);
		}

		#endregion

		#region Delete
		public virtual ActionResult Delete(int id = 0)
		{
			var model = GetModelBy(id);

			if (model == null)
			{
				throw new HttpException(404, "Not Found");
			}

			SetModelProperties(model);

			return View(model);
		}

		[HttpPost]
		[ValidateInput(false)]
		[ValidateAntiForgeryToken]
		public virtual ActionResult Delete(TDTO dto)
		{
			try
			{

				ServiceDelete(dto);
				//Se guarda el mensaje de exito para que no se pierda
				TempData["SuccessMessage"] = Localizator.GetStringResourceWithFormat("Admin", "Delete_Success");
				return RedirectToAction("Index");
			}
			catch (ValidationRepositoryException ex)
			{
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (GeneralException ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (Exception ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			return View(dto);

		}

		/// <summary>
		/// Debe ser sobreescrito si el controlador requiere la funcionalidad de borrado
		/// </summary>
		/// <param name="id"></param>
		protected virtual void ServiceDelete(TDTO dto)
		{
			throw new HttpException(404, "Not Found");
		}

		#endregion

		/// <summary>
		/// Este metodo no es necesario sobreescribirlo si lo unico que se requiere
		/// es crear una instancia del ViewModel
		/// </summary>
		/// <returns></returns>
		protected virtual TDTO GetNewModel(int id)
		{
			return new TDTO();
		}

		/// <summary>
		/// Sobreescribir en caso de que se requiera "settear" alguna propiedad al ViewModel.
		/// Se llama siempre que se entra a una pagina de Creacion/Edicion y luego de guardar
		/// (exitosa o fallidamente) los datos del formulario que digito el usuario
		/// </summary>
		/// <param name="model"></param>
		protected virtual void SetModelProperties(TDTO model)
		{
			//el comportamiento por defecto es no hacer nada
		}

	}

	public abstract class BaseVwController<TDTO, TVwDTO> : BaseController<TDTO>
		where TDTO : class, new()
		where TVwDTO : class, new()
	{

		private IVwBaseService<TVwDTO> _vwService;
		public BaseVwController(IBaseService<TDTO> service, IVwBaseService<TVwDTO> vwservice)
			: base(service)
		{
			this._vwService = vwservice;
		}

		#region Read


		public ActionResult GetVwAll([DataSourceRequest]DataSourceRequest request)
		{
			return Json(_vwService.ExecuteVwKendoResult(request), JsonRequestBehavior.AllowGet);
		}


		#endregion


	}
}