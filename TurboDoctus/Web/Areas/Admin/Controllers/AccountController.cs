﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using App.Domain.DTO.Users;
using App.Web.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using App.Domain.Services.Users;
using doct.Exception;
using doct.Extensions;
using App.Common.Classes.Extensions;
using doct.Localization;
using App.Common.Localization;

namespace App.Web.Areas.Admin.Controllers
{
	//[Authorize(Roles = "Administrador")]
	public class AccountController : Controller
	{
		private readonly IUserService _userService;
		private readonly IRoleService _roleService;
		private ILocalizationManager _localizator;

		public ILocalizationManager Localizator
		{
			get
			{
				return _localizator ?? (_localizator = new LocalizationManager());
			}

		}

		public AccountController(IUserService userService, IRoleService roleService)
		{
			_userService = userService;
			_roleService = roleService;
		}

		//
		// GET: /Account/Login
		[AllowAnonymous]
		public ActionResult Login(string returnUrl)
		{
			ViewBag.ReturnUrl = returnUrl;
			return View();
		}

		//
		// POST: /Account/Login
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Login(LoginDTO model, string returnUrl)
		{
			try
			{
				var identity = await _userService.GetClaimsFromUserIfExistAsync(model, DefaultAuthenticationTypes.ApplicationCookie);
				AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
				AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = model.RememberMe }, identity);
				return RedirectToLocal(returnUrl);
			}
			catch (ValidationRepositoryException ex)
			{
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (GeneralException ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (ApplicationException e)
			{
				ViewBag.ErrorMessage = e.ToUlHtmlString();
			}
			catch (Exception ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}

			return View(model);

		}

		//
		// POST: /Account/LogOff
		[HttpPost]
		[ValidateAntiForgeryToken]
		[AllowAnonymous]
		public ActionResult LogOff()
		{
			AuthenticationManager.SignOut();
			return RedirectToAction("Index", "Home");
		}


		//GET: /Account/Manage
		public async Task<ActionResult> Manage()
		{
			ViewBag.SuccessMessage = TempData["SuccessMessage"];
			ViewBag.HasLocalPassword = await _userService.HasLocalPasswordAsync(User.Identity.GetUserId().GetInt());
			ViewBag.ReturnUrl = Url.Action("Manage");
			return View();
		}


		//POST: /Account/Manage
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Manage(ChangePasswordDTO model)
		{
			bool hasPassword = await _userService.HasLocalPasswordAsync(User.Identity.GetUserId().GetInt());
			ViewBag.HasLocalPassword = hasPassword;
			ViewBag.ReturnUrl = Url.Action("Manage");
			try
			{
				model.UserId = User.Identity.GetUserId().GetInt();
				await _userService.ChangePasswordAsync(model, hasPassword);
				TempData["SuccessMessage"] = Localizator.GetStringResourceWithFormat("Admin", "Change_Password");
				return RedirectToLocal(ViewBag.ReturnUrl);
			}
			catch (ValidationRepositoryException ex)
			{
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (GeneralException ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (ApplicationException e)
			{
				ViewBag.ErrorMessage = e.ToUlHtmlString();
			}
			catch (Exception ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}

			return View(model);
		}

		#region Create

		public virtual ActionResult Create(int id = 0)
		{
			var model = new UserDTO();

			SetModelProperties(model);

			return View(model);
		}

		[HttpPost]
		[ValidateInput(false)]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Create(UserDTO model, int id = 0)
		{
			try
			{
				await _userService.CreateAsync(model);
				TempData["SuccessMessage"] = Localizator.GetStringResourceWithFormat("Admin", "Insert_Success");
				return RedirectToAction("Index");
			}
			catch (ValidationRepositoryException e)
			{
				ViewBag.ErrorMessage = e.ToUlHtmlString();
			}
			catch (ApplicationException e)
			{
				ViewBag.ErrorMessage = e.ToUlHtmlString();
			}
			catch (GeneralException ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (Exception ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			SetModelProperties(model);

			return View(model);
		}



		#endregion

		#region Read

		public virtual ActionResult Index(int id = 0)
		{
			ViewBag.SuccessMessage = TempData["SuccessMessage"];
			return View();
		}

		public virtual ActionResult GetAll([DataSourceRequest]DataSourceRequest request)
		{
			return Json(_userService.ExecuteKendoResult(request), JsonRequestBehavior.AllowGet);
		}


		public ActionResult GetDataToDropDown()
		{
			return Json(_userService.GetAll(), JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region Update

		public async Task<ActionResult> Edit(int id = 0)
		{
			var model = await _userService.FindByIdAsync(id);

			if (model == null)
			{
				throw new HttpException(404, "Not Found");
			}

			SetModelProperties(model);

			return View(model);
		}

		[HttpPost]
		[ValidateInput(false)]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Edit(int id, UserDTO model)
		{
			try
			{
				await _userService.UpdateAsync(id, model);
				TempData["SuccessMessage"] = Localizator.GetStringResourceWithFormat("Admin", "Update_Success");
				return RedirectToAction("Index");
			}
			catch (ValidationRepositoryException ex)
			{
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (GeneralException ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (ApplicationException e)
			{
				ViewBag.ErrorMessage = e.ToUlHtmlString();
			}
			catch (Exception ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}

			SetModelProperties(model);

			return View(model);
		}


		#endregion

		#region Delete
		public async Task<ActionResult> Delete(int id = 0)
		{
			var model = await _userService.FindByIdAsync(id);

			return View(model);
		}

		[HttpPost]
		[ValidateInput(false)]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Delete(UserDTO dto)
		{
			try
			{

				await _userService.DeleteAsync(dto.Id);
				//Se guarda el mensaje de exito para que no se pierda
				TempData["SuccessMessage"] = Localizator.GetStringResourceWithFormat("Admin", "Delete_Success");
				return RedirectToAction("Index");
			}
			catch (ValidationRepositoryException ex)
			{
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (GeneralException ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			catch (Exception ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				ViewBag.ErrorMessage = ex.ToUlHtmlString();
			}
			return View(dto);

		}



		#endregion



		[ChildActionOnly]
		public async Task<ActionResult> RemoveAccountList()
		{
			var linkedAccounts = await _userService.GetLoginsAsync(User.Identity.GetUserId().GetInt());
			ViewBag.ShowRemoveButton = await _userService.HasLocalPasswordAsync(User.Identity.GetUserId().GetInt()) || linkedAccounts.Count > 1;
			return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
		}


		#region Helpers
		// Used for XSRF protection when adding external logins
		private const string XsrfKey = "XsrfId";

		private IAuthenticationManager AuthenticationManager
		{
			get
			{
				return HttpContext.GetOwinContext().Authentication;
			}
		}


		private ActionResult RedirectToLocal(string returnUrl)
		{
			if (Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}
			else
			{
				return RedirectToAction("Index", "Home");
			}
		}

		protected void SetModelProperties(UserDTO model)
		{
			if (model.Roles == null)
			{
				model.Roles = _roleService.GetAll();
			}
		}


		#endregion
	}

}