﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Base
{
	public interface IResult<T>
	{

		//object Result(IQueryable<T> querable);

		object Result(IQueryable<T> querable, object request);

	}
}
