﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Classes.Utilities.DTO
{
    public class HeaderDTO
    {
        public ResponseCodeDTO ReponseCode { get; set; }

        List<string> _Messages;
        public List<string> Messages
        {
            get
            {
                if (_Messages == null)
                {
                    _Messages = new List<string>();
                }
                return _Messages;
            }
            
        }

        public bool Success
        {
            get
            {
                var code = (int)this.ReponseCode;

                if (code >= 200 && code < 300)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
