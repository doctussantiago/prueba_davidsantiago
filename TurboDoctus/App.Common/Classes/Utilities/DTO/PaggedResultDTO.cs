﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes.Utilities.DTO
{
    [Serializable]
    public class PaggedResultDTO<T>
    {
        public int Count { get; set; }
        public List<T> List { get; set; }
    }
}
