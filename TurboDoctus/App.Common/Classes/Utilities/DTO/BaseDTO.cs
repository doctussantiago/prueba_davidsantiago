﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes.Utilities.DTO
{
    public class BaseDTO<TEntity, TDTO> where TDTO : class,new()
    {
        public virtual TEntity ParseDTO(TDTO dto)
        {
            throw new NotImplementedException();
        }
        public virtual TDTO ParseEntity(TEntity entity)
        {
            throw new NotImplementedException();
        }
        public virtual List<TEntity> ParseDTOs(List<TDTO> dto)
        {
            throw new NotImplementedException();
        }
        public virtual List<TDTO> ParseEntities(List<TEntity> entity)
        {
            throw new NotImplementedException();
        }

        public static string SerializeToEmptyJson()
        {

            TDTO dto = new TDTO();
            return JsonConvert.SerializeObject(dto);
        }

        public string SerializeToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

    }
}
