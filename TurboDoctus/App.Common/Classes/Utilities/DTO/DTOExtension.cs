﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes.Utilities.DTO
{
    public static class DTOExtension
    {

        public static ResponseDTO ToResultDTO<TEntity,TDTO>(this List<TEntity> entities, int count) where TDTO : BaseDTO<TEntity, TDTO>, new()
        {

            ResponseDTO response = new ResponseDTO();

            //El header
            HeaderDTO headerDTO = new HeaderDTO();
            headerDTO.ReponseCode = ResponseCodeDTO.OK;
            response.Header = headerDTO;

            //Los datos            
            PaggedResultDTO<TDTO> pagedResult = new PaggedResultDTO<TDTO>();
            pagedResult.Count = count;
            pagedResult.List = (new TDTO()).ParseEntities(entities);
            response.Data = pagedResult;
            

            return response;
        }


        public static ResponseDTO ToResultDTO<TEntity, TDTO>(this TEntity entity) where TDTO : BaseDTO<TEntity, TDTO>, new()
        {

            ResponseDTO response = new ResponseDTO();

            //El header
            HeaderDTO headerDTO = new HeaderDTO();
            headerDTO.ReponseCode = ResponseCodeDTO.OK;
            response.Header = headerDTO;

            //Los datos            
            response.Data = entity;


            return response;
        }


        public static ResponseDTO ToResultDTO(this Exception exception)
        {

            string message = "Ha ocurrido un error, por favor intentarlo mas tarde";
            if (exception is ApplicationException) {
                message = exception.Message;
            }

            ResponseDTO response = new ResponseDTO();

            //El header
            response.Header.ReponseCode = ResponseCodeDTO.ERROR;
            response.Header.Messages.Add(message);
            

            //Los datos            
            response.Data = null;


            return response;
        }


    }
}
