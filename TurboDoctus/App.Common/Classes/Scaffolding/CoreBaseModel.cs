﻿using Core.Classes.Factory;
using doct.Scaffolding.MVC.Model;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes.Scaffolding
{
	//public class CoreBaseModel<TEntity> : BaseModel<Core.Model.DataBase.CoreDbContext, TEntity> where TEntity : class
	//{
	//	#region CREATE
	//	public new TEntity Create(TEntity entity)
	//	{
	//		//TODO: Ver como hacer para que CreationDate y ModificationDate son tenidos en cuenta
	//		//entity.CreationDate = entity.ModificationDate = DateTimeHelper.GetCurrentColombianTime();


	//		CoreAbstractValidation<TEntity> valitaror = DependencyValidationFactory.CreateInstance<TEntity>();

	//		if (valitaror != null)
	//		{
	//			valitaror.LoadCreateRules();
	//			ValidationResult valResult = valitaror.Validate(entity);

	//			if (!valResult.IsValid)
	//			{

	//				throw new ValidationException(valResult.Errors);
	//			}
	//		}

	//		return base.Create(entity);
	//	}
	//	#endregion

	//	#region EDIT
	//	public new TEntity Edit(TEntity entity, TEntity originalEntity)
	//	{
	//		//TODO: Organizar lo del creation date y modification date
	//		//entity.CreationDate = original.CreationDate;
	//		//entity.ModificationDate = DateTimeHelper.GetCurrentColombianTime();

	//		CoreAbstractValidation<TEntity> valitaror = DependencyValidationFactory.CreateInstance<TEntity>();
	//		valitaror.LoadEditRules();
	//		ValidationResult valResult = valitaror.Validate(entity);

	//		if (!valResult.IsValid)
	//		{
	//			throw new ValidationException(valResult.Errors);
	//		}

	//		return base.Edit(entity, originalEntity);
	//	}
	//	#endregion

	//	#region DELETE
	//	public new void Delete(TEntity entity)
	//	{
	//		CoreAbstractValidation<TEntity> valitaror = DependencyValidationFactory.CreateInstance<TEntity>();
	//		valitaror.LoadDeleteRules();

	//		ValidationResult valResult = valitaror.Validate(entity);

	//		if (!valResult.IsValid)
	//		{
	//			throw new ValidationException(valResult.Errors);
	//		}

	//		base.Delete(entity);
	//	}
	//	#endregion

	//}
}
