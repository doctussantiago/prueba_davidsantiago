﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes.Scaffolding
{
    public abstract class CoreAbstractValidation<TEntity> : AbstractValidator<TEntity>
    {
        public abstract void LoadCreateRules();

        public abstract void LoadEditRules();

        public abstract void LoadDeleteRules();
    }

}
