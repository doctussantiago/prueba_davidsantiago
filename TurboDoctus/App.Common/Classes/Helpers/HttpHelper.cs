﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;

namespace App.Common.Helpers
{
	public static class HttpHelper
	{
		static HttpFacade _HttpFacade;
		public static HttpFacade HttpFacade
		{
			get
			{
				if (_HttpFacade == null)
				{
					_HttpFacade = new HttpFacade();
				}
				return _HttpFacade;
			}
			set
			{
				_HttpFacade = value;
			}
		}


		/// <summary>
		/// Obtiene la IP del cliente que esta realizando la llamada HTTP.
		/// </summary>
		/// <param name="request">El HttpRequest del cual se quiere sacar la IP</param>
		/// <returns>La IP del cliente. Si no se puede adquirir la IP se devuelve el valor "0.0.0.0"</returns>
		public static string GetIPAddress(HttpRequest request)
		{
			string ip = request.ServerVariables["REMOTE_ADDR"];
			if (String.IsNullOrEmpty(ip))
			{
				ip = "0.0.0.0";
			}
			return ip;
		}

		/// <summary>
		/// Obtiene la IP del cliente que esta realizando la llamada HTTP utilizando el HttpRequest que este en el HttpContext actual.
		/// </summary>
		/// <returns>La IP del cliente. Si no se puede adquirir la IP se devuelve el valor "0.0.0.0"</returns>
		public static string GetIpAddress()
		{
			//HttpRequest request = HttpContext.Current.Request;
			//return GetIPAddress(request);

			return HttpFacade.GetIpAddress();
		}

		/// <summary>
		/// Convierte un URL relativo en un URL absoluto. Ej. El contexto del dominio: www.dominio.com, Ruta Relativa: ~/temp/prueba.aspx, Resultado: www.dominio.com/temp/prueba.aspx
		/// </summary>
		/// <param name="request">El HttpRequest del cual se quiere sacar la ruta absoluta</param>
		/// <param name="retativeURL">La ruta URL que se quiere convertir en una ruta URL absoluta</param>
		/// <returns>La URL absoluta</returns>
		public static string ResolveAbsoluteUrl(HttpRequest request, string relativeURL)
		{
			Control control = new Control();
			string path = string.Empty;
			//if (request.Url.Port == 80)
			//{
			//	path = string.Format("http://{0}{1}", request.Url.Host, control.ResolveUrl(relativeURL));
			//}
			//else if (request.Url.Port == 443)
			//{
			//	path = string.Format("https://{0}{1}", request.Url.Host, control.ResolveUrl(relativeURL));
			//}
			//else
			//{
			//	path = string.Format("http://{0}:{1}{2}", request.Url.Host, request.Url.Port, control.ResolveUrl(relativeURL));
			//}


			if (request.Url.Port == 80 || request.Url.Port == 443)
			{
				path = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Host, control.ResolveUrl(relativeURL));
			}
			else
			{
				path = string.Format("{0}://{1}:{2}{3}", request.Url.Scheme, request.Url.Host, request.Url.Port, control.ResolveUrl(relativeURL));
			}

			return path;
		}

		/// <summary>
		/// Convierte un URL relativo en un URL absoluto utilizando el HttpRequest que este en el HttpContext actual. Ej. El contexto del dominio: www.dominio.com, Ruta Relativa: ~/temp/prueba.aspx, Resultado: www.dominio.com/temp/prueba.aspx
		/// </summary>
		/// <param name="retativeURL">La ruta URL que se quiere convertir en una ruta URL absoluta</param>
		/// <returns>La URL absoluta</returns>
		public static string ResolveAbsoluteUrl(string relativeURL)
		{
			HttpRequest request = HttpContext.Current.Request;
			return ResolveAbsoluteUrl(request, relativeURL);
		}
	}
}
