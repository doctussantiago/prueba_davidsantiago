﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace App.Common.Classes.Helpers
{
	public static class FileHelper
	{

		public static string SaveFile(HttpContextBase httpContext, string inputFieldName, string targetDirectory)
		{
			var uploadedFile = httpContext.Request.Files[inputFieldName];

			if (uploadedFile != null)
			{
				string fileName = uploadedFile.FileName;

				string[] pathIE = fileName.Split('\\');

				if (pathIE.Count() > 1)
				{
					fileName = pathIE.LastOrDefault();
				}

				targetDirectory = System.Web.Hosting.HostingEnvironment.MapPath(targetDirectory);

				fileName = CalculateFileName(fileName, targetDirectory);

				uploadedFile.SaveAs(targetDirectory + fileName);

				return fileName;
			}

			return null;
		}

		public static string CalculateFileName(string fileName, string targetDirectory)
		{
			var extension = Path.GetExtension(fileName);
			//Se coloca un nombre que no contenga caracteres raros. 
			//Anteriormente se usaba Path.GetFileNameWithoutExtension(fileFullPath); 
			//pero al tener caracteres como ( o espacios en blanco 
			//etc genera problemas si se quiere guardar en repositorios como el s3 de amazon
			var fileWithoutExtension = fileName.Replace(extension, string.Empty);
			fileName = string.Format("{0}{1}", Regex.Replace(fileWithoutExtension, @"[^0-9a-zA-Z]+", string.Empty), extension);
			var targerFilePath = targetDirectory + fileName;
			while (System.IO.File.Exists(targerFilePath))
			{
				fileName = "1" + fileName;
				targerFilePath = targetDirectory + fileName;
			}
			return fileName;
		}

		public static string GetContentType(string filename)
		{
			string contentType = string.Empty;

			//documentos
			if (filename.Contains(".pdf"))
			{
				contentType = "application/pdf";
			}
			else if (filename.Contains(".docx"))
			{
				contentType = "application/docx";
			}
			else if (filename.Contains(".xls"))
			{
				contentType = "application/excel";
			}

				//imagenes
			else if (filename.Contains(".jpg") || filename.Contains(".jpeg"))
			{
				contentType = "image/jpeg";
			}
			else if (filename.Contains(".png"))
			{
				contentType = "image/png";
			}
			else if (filename.Contains(".gif"))
			{
				contentType = "image/gif";
			}

			//varios
			else if (filename.Contains(".zip"))
			{
				contentType = "application/zip";
			}
			else if (filename.Contains(".mp3"))
			{
				contentType = "video/mpeg";
			}



			return contentType;
		}


	}
}
