﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace App.Common.Classes.Helpers
{
    public static class AreaHelper
    {
        public static void RegisterArea<T>(RouteCollection routes, object state) where T : AreaRegistration
        {
            AreaRegistration registration = (AreaRegistration)Activator.CreateInstance(typeof(T));
            AreaRegistrationContext context = new AreaRegistrationContext(registration.AreaName, routes, state);
            string tNamespace = registration.GetType().Namespace;
            if (tNamespace != null)
            {
                context.Namespaces.Add(tNamespace + ".*");
            }
            registration.RegisterArea(context);
        }
    }
}
