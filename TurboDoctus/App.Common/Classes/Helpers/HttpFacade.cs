﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace App.Common.Helpers
{
	public class HttpFacade
	{
		public virtual string GetIpAddress()
		{
			HttpRequest request = HttpContext.Current.Request;
			return GetIPAddress(request);
		}

		public string GetIPAddress(HttpRequest request)
		{
			string ip = request.ServerVariables["REMOTE_ADDR"];
			if (String.IsNullOrEmpty(ip))
			{
				ip = "0.0.0.0";
			}
			return ip;
		}

		public virtual bool IsUserAuthenticated()
		{
			return HttpContext.Current.User.Identity.IsAuthenticated;
		}

		public virtual string GetIdentityName()
		{
			return HttpContext.Current.User.Identity.Name;
		}
	}
}
