﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Classes.Validation
{
	public interface IValidatorEntity<TEntity> where TEntity : class
	{
		void InsertPreValidation(TEntity entity);
		void InsertPostValidation(TEntity entity);
		void DeletePreValidation(TEntity entity);
		void DeletePostValidation(TEntity entity);
		void EditPreValidation(TEntity entity);
		void EditPostValidation(TEntity entity);
	}
}
