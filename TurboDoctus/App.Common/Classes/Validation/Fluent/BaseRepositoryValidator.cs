﻿using doct.Exception;
using doctFramework.Validation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;

namespace App.Common.Classes.Validation.Fluent
{
	public abstract class BaseRepositoryValidator<T> : IRepositoryValidator<T> where T : class
	{
		protected LocalValidator<T> PreInsertValidator { get; set; }
		protected LocalValidator<T> PostInsertValidator { get; set; }

		protected LocalValidator<T> PreUpdateValidator { get; set; }
		protected LocalValidator<T> PostUpdateValidator { get; set; }

		protected LocalValidator<T> PreDeleteValidator { get; set; }
		protected LocalValidator<T> PostDeleteValidator { get; set; }



		public BaseRepositoryValidator()
		{
			PreInsertValidator = new LocalValidator<T>();
			PostInsertValidator = new LocalValidator<T>();
			PreUpdateValidator = new LocalValidator<T>();
			PostUpdateValidator = new LocalValidator<T>();
			PreDeleteValidator = new LocalValidator<T>();
			PostDeleteValidator = new LocalValidator<T>();

			LoadPreInsertRules();
			LoadPostInsertRules();
			LoadPreUpdateRules();
			LoadPostUpdateRules();
			LoadPreDeleteRules();
			LoadPostDeleteRules();
		}

		public abstract void LoadPreInsertRules();
		public abstract void LoadPostInsertRules();

		public abstract void LoadPreUpdateRules();
		public abstract void LoadPostUpdateRules();

		public abstract void LoadPreDeleteRules();
		public abstract void LoadPostDeleteRules();



		private void ValidateResult(ValidationResult result)
		{
			if (!result.IsValid)
			{
				List<string> errors = new List<string>();
				foreach (ValidationFailure valResult in result.Errors)
				{
					errors.Add(valResult.ErrorMessage);
				}
				throw new ValidationRepositoryException(errors);
			}
		}


		public void ValidatePreInsert(T entity)
		{
			ValidateResult(PreInsertValidator.Validate(entity));
		}

		public void ValidatePostInsert(T entity)
		{
			ValidateResult(PostInsertValidator.Validate(entity));
		}


		public void ValidatePreUpdate(T entity)
		{
			ValidateResult(PreUpdateValidator.Validate(entity));
		}

		public void ValidatePostUpdate(T entity)
		{
			ValidateResult(PostUpdateValidator.Validate(entity));
		}

		public void ValidatePreDelete(T entity)
		{
			ValidateResult(PreDeleteValidator.Validate(entity));
		}

		public void ValidatePostDelete(T entity)
		{
			ValidateResult(PostDeleteValidator.Validate(entity));
		}

		public virtual bool BeAValidDate(DateTime date)
		{
			return !(date.Equals(default(DateTime)));
		}
	}


}
