﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes.Enums
{
    public enum ResponseEnum
    {
        Ok = 200,
        ValidationError = 400,
        ValidationErrorNotFound = 404,
        ServerError = 500
    }
}
