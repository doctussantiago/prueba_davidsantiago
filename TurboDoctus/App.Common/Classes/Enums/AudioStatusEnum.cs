﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes.Enums
{
    public enum AudioStatusEnum
    {
        Pendiente = 1,
        Aprobado = 2,
        Rechazado = 3
    }
}
