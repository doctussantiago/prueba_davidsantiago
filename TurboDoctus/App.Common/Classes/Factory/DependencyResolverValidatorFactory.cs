﻿using FluentValidation;
using StructureMap;
using StructureMap.Graph;
using System;


namespace Core.Classes.Factory
{
    public class DependencyResolverValidatorFactory : ValidatorFactoryBase
    {
        public override IValidator CreateInstance(Type validatorType)
        {

            //Se hace el automappeo de las validaciones
            Container container = new Container(c =>
                {
                    c.Scan(s =>
                    {
                        s.TheCallingAssembly(); //Incluimos el Assembly para que pueda buscar
                        s.IncludeNamespace("Core"); //Es importante decir cual namespace utiliza
                        s.ConnectImplementationsToTypesClosing(typeof(IValidator<>)); //Le decimos que tipo buscar

                        //Si no se ponen los 3 no funciona. Intente varias maneras 
                        //y vi la documentación pero este es el unico que me funciono 
                        //para que fuera automatico el mapeo de las clases

                    });

                }
            );

            //Se hace con 
            return container.TryGetInstance(validatorType) as IValidator;
        }
    }
}
