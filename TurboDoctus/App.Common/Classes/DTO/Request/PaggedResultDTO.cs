﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes.DTO.Request
{
    public class PaggedResultDTO<T>
    {
        public int FullResultCount { get; set; }
        public T Result { get; set; }
    }
}
