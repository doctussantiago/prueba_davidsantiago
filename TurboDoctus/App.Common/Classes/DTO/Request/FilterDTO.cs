﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes.DTO.Request
{
    public class FilterDTO
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string Filter { get; set; }
        public string Type { get; set; }
        public string EntityId { get; set; }
    }
}
