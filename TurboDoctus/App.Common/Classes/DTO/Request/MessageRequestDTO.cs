﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes.DTO.Request
{
    public class MessageRequestDTO
    {
        public string Mail { get; set; }
        public string Message { get; set; }
    }
}
