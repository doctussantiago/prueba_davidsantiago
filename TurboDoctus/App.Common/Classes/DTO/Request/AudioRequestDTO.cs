﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes.DTO.Request
{
    public class AudioRequestDTO
    {
        public string Mail { get; set; }
        public string Base64 { get; set; }
    }
}
