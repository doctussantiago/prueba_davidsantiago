//-------------------------------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by EntitiesToDTOs.v3.2 (entitiestodtos.codeplex.com).
//     Timestamp: 2015/04/23 - 13:43:58
//
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//-------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Core.Classes.DTO.Entities
{
    [DataContract()]
    public partial class AudioDTO
    {
        [DataMember()]
        public Int32 AudioId { get; set; }

        [DataMember()]
        public Int32 AudioStatusId { get; set; }

        [DataMember()]
        public Int32 UserId { get; set; }

        [DataMember()]
        public DateTime CreationDate { get; set; }

        [DataMember()]
        public DateTime ModificationDate { get; set; }

        [DataMember()]
        public String AudioBase64 { get; set; }

        [DataMember()]
        public Int32 AudioStatus_AudioStatusId { get; set; }

        [DataMember()]
        public Int32 User_UserId { get; set; }

        public AudioDTO()
        {
        }

        public AudioDTO(Int32 audioId, Int32 audioStatusId, Int32 userId, DateTime creationDate, DateTime modificationDate, String audioBase64, Int32 audioStatus_AudioStatusId, Int32 user_UserId)
        {
			this.AudioId = audioId;
			this.AudioStatusId = audioStatusId;
			this.UserId = userId;
			this.CreationDate = creationDate;
			this.ModificationDate = modificationDate;
			this.AudioBase64 = audioBase64;
			this.AudioStatus_AudioStatusId = audioStatus_AudioStatusId;
			this.User_UserId = user_UserId;
        }
    }
}
