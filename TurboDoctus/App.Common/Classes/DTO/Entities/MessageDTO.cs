//-------------------------------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by EntitiesToDTOs.v3.2 (entitiestodtos.codeplex.com).
//     Timestamp: 2015/04/23 - 13:43:58
//
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//-------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Core.Classes.DTO.Entities
{
    [DataContract()]
    public partial class MessageDTO
    {
        [DataMember()]
        public Int32 MessageId { get; set; }

        [DataMember()]
        public Int32 UserId { get; set; }

        [DataMember()]
        public String MessageText { get; set; }

        [DataMember()]
        public DateTime CreationDate { get; set; }

        [DataMember()]
        public DateTime ModificationDate { get; set; }

        [DataMember()]
        public Int32 User_UserId { get; set; }

        public MessageDTO()
        {
        }

        public MessageDTO(Int32 messageId, Int32 userId, String messageText, DateTime creationDate, DateTime modificationDate, Int32 user_UserId)
        {
			this.MessageId = messageId;
			this.UserId = userId;
			this.MessageText = messageText;
			this.CreationDate = creationDate;
			this.ModificationDate = modificationDate;
			this.User_UserId = user_UserId;
        }
    }
}
