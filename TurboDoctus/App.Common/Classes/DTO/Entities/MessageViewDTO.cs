﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes.DTO.Entities
{
    public class MessageViewDTO
    {
        public int MessageId { get; set; }
        public int UserId { get; set; }
        public String FullName { get; set; }
        public String DocumentId { get; set; }
        public String PhoneNumber { get; set; }
        public String Mail { get; set; }
        public String CityName { get; set; }
        public string Message { get; set; }

        public string Date { get; set; }

        public string MotherName { get; set; }
        public string MotherCity { get; set; }
    }
}
