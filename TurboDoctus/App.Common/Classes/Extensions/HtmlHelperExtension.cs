﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;
using System.Web.Mvc;

namespace App.Common.Classes.Extensions
{
	public static class HtmlHelperExtension
	{

		public static GridBuilder<TModel> CoreKendoGrid<TModel>(this HtmlHelper helper, string action, string controller) where TModel : class
		{

			return helper.Kendo().Grid<TModel>()
				.Name("grid")
				.Pageable()
				.Sortable()
				.Excel(excel => excel
					.AllPages(true)
					.Filterable(true)
					)
				.Filterable(f =>
					f.Messages(a =>
						a.And("Y").Cancel("Cancelar").CheckAll("Marcar todas").Clear("Limpiar").Filter("Buscar").Info("Buscar registro(s) donde:").IsFalse("Está inactivo").IsTrue("Está activo").Operator("Operador").Or("O").SelectValue("Seleccionar valor").Value("Valor"))
					.Operators(o =>
						o.ForString(x => x.IsEqualTo("Es igual a").Contains("Contiene").DoesNotContain("No contiene").EndsWith("Termina con").IsNotEqualTo("No es igual").StartsWith("Empieza con"))
						.ForNumber(x => x.IsEqualTo("Es igual a").IsNotEqualTo("No es igual").IsGreaterThan("Es mayor a").IsGreaterThanOrEqualTo("Es mayor o igual a").IsLessThan("Es menor a").IsLessThanOrEqualTo("Es menor o igual a"))
						.ForDate(x => x.IsEqualTo("Es igual a").IsNotEqualTo("No es igual").IsGreaterThan("Es mayor a").IsGreaterThanOrEqualTo("Es mayor o igual a").IsLessThan("Es menor a").IsLessThanOrEqualTo("Es menor o igual a"))
						.ForEnums(x => x.IsEqualTo("Es igual a").IsNotEqualTo("No es igual"))))
				.DataSource(dataSource => dataSource
					.Ajax()
					.PageSize(10)
					.Read(read => read.Action(action, controller))
				);
		}


		public static MvcHtmlString CoreDropDownMenu(this HtmlHelper helper, IEnumerable<string> actions)
		{
			string lis = "";

			foreach (var item in actions)
			{
				lis += string.Format("<li>{0}</li>", item);
			}

			string baseString = string.Format(
		  "<div class='btn-group' style='position:relative'>" +
		  "<button class='btn btn-sm'>Acciones</button>" +
		  "<button class='btn dropdown-toggle btn-sm' data-toggle='dropdown'><span class='caret'></span></button>" +
		  "<ul class='dropdown-menu' style='position:absolute' >" +
		  "{0}" +
		  "</ul>" +
		  "</div>", lis);

			return (new MvcHtmlString(baseString));
		}


	}
}
