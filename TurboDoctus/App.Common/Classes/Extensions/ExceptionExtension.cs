﻿using doct.Exception;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace App.Common.Classes.Extensions
{
	public static class ExceptionExtension
	{

		public static MvcHtmlString ToUlHtmlString(this Exception ex)
		{
			Type type = ex.GetType();
			string validationExceptionFullName = typeof(FluentValidation.ValidationException).FullName;

			StringBuilder sb = new StringBuilder();
			sb.Append("<ul>");

			switch (type.FullName)
			{
				case "FluentValidation.ValidationException":

					ValidationException valEx = ex as ValidationException;
					foreach (var item in valEx.Errors)
					{
						sb.Append(string.Format("<li>{0}</li>", item.ErrorMessage));
					}
					break;
				default:
					sb.Append(string.Format("<li>{0}</li>", "Ha ocurrido un error, por favor intentarlo mas tarde"));
					break;
			}

			sb.Append("</ul>");

			return (new MvcHtmlString(sb.ToString()));

		}

		public static MvcHtmlString ToUlHtmlString(this ValidationRepositoryException ex)
		{

			StringBuilder sb = new StringBuilder();
			sb.Append("<ul>");

			foreach (var item in ex.ErrorMessages)
			{
				sb.Append(string.Format("<li>{0}</li>", item));
			}

			sb.Append("</ul>");

			return (new MvcHtmlString(sb.ToString()));

		}

	}
}
