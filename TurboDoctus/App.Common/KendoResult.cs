﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using AutoMapper;
using App.Common.Base;

namespace App.Common
{
	public class KendoResult
	{


		public KendoResult()
		{
		}

		public object Result<TInput, TOutput>(IQueryable<TInput> queryable, object request) where TInput : class
		{
			return queryable.ToDataSourceResult(request as DataSourceRequest, data => Mapper.DynamicMap<TOutput>(data));
		}
	}
}
