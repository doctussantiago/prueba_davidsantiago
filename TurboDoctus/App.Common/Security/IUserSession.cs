﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Security
{
	public interface IUserSession
	{
		string UserName { get; }

		string IP { get; }

	}
}
