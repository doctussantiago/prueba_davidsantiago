﻿using doct.Exception;
using doct.Localization;
using System.Web;

namespace App.Common.Localization
{
	public class LocalizationManager : ILocalizationManager
	{

		public string GetStringResource(string classkey, string resourceKey)
		{
			try
			{
				//TODO: Tirar excepcion sino encuentra el resource
				return HttpContext.GetGlobalResourceObject(classkey, resourceKey) as string;
			}
			catch (System.Resources.MissingManifestResourceException ex)
			{

				throw new GeneralException(ex.Message);
			}

		}


		public string GetStringResourceWithFormat(string classkey, string resourceKey, params string[] formatParams)
		{
			try
			{
				string format = GetStringResource(classkey, resourceKey) ?? string.Empty;

				//TODO: Aqui se debe validar que no tire excepcion cuando se manden mas parametros de los que tiene el string.
				string resourceString = string.Format(format, formatParams);
				return resourceString;
			}
			catch (System.ArgumentNullException ex)
			{
				throw new GeneralException(ex.Message);
			}
			catch (System.FormatException ex)
			{
				throw new GeneralException(ex.Message);
			}

		}


	}
}
