﻿using App.Infrastructure.Model.Model;
using doctFramework.Repository;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System;
using App.Infrastructure.Model.SecurityEntities;

namespace App.Infrastructure.Repository.Core.Users
{
	public interface IUserRepository : IUserLoginStore<IdentityUser, int>, IUserClaimStore<IdentityUser, int>, IUserRoleStore<IdentityUser, int>, IUserPasswordStore<IdentityUser, int>, IUserSecurityStampStore<IdentityUser, int>, IUserStore<IdentityUser, int>, IQueryableUserStore<IdentityUser, int>, IDisposable
	{

	}
}
