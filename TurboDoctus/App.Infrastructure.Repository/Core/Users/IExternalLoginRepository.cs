﻿
using App.Infrastructure.Model.Model;
using System.Threading;
using System.Threading.Tasks;
using doctFramework.Repository;

namespace App.Infrastructure.Repository.Core.Users
{
	public interface IExternalLoginRepository : IRepository<ExternalLogin>
	{
		ExternalLogin GetByProviderAndKey(string loginProvider, string providerKey);
		Task<ExternalLogin> GetByProviderAndKeyAsync(string loginProvider, string providerKey);
		Task<ExternalLogin> GetByProviderAndKeyAsync(CancellationToken cancellationToken, string loginProvider, string providerKey);
	}
}
