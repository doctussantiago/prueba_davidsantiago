﻿using App.Infrastructure.Model.Model;
using doctFramework.Repository;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using App.Infrastructure.Model.SecurityEntities;
using System;



namespace App.Infrastructure.Repository.Core.Users
{
	public interface IRoleRepository : IRoleStore<IdentityRole, int>, IQueryableRoleStore<IdentityRole, int>, IDisposable
	{

	}
}
