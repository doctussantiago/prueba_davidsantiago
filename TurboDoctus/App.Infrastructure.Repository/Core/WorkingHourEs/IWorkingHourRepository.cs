﻿using App.Infrastructure.Model.Model;
using doctFramework.Repository;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infrastructure.Repository.Core.WorkingHourEs
{
    public interface IWorkingHourRepository : IRepository<WorkingHour>
    {
    }
}
