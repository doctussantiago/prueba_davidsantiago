﻿using App.Infrastructure.Model.Model;
using doctFramework.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infrastructure.Repository.Core.Employees
{
    public interface IEmployeRepository : IRepository<Employee>
    {
    }
}

