﻿using App.Infrastructure.EntityFramework;
using App.Infrastructure.Model.Model;
using App.Infrastructure.Repository.Core;
using App.Infrastructure.Model.SecurityEntities;
using App.Infrastructure.Repository.EntityFramework.Users;
using Autofac;
using Autofac.Core;
using Doct.EFLogStorage;
using System.Data.Entity;
using Microsoft.AspNet.Identity;

namespace App.Infrastructure.Repository.Config
{
    public class AutofacConfig
	{
        public static void RegisterTypes(ContainerBuilder builder)
    	{
            builder.RegisterType<Entities>().As<Entities>()
			.Named("entities", typeof(Entities)).InstancePerRequest();

			builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().WithParameters(new[] {
			new ResolvedParameter((p,c) => p.Name == "context",(p,c) => c.ResolveNamed<Entities>("entities"))
			});
            
            builder.RegisterType<PasswordIdentityValidator>().As<IIdentityValidator<string>>();
			builder.RegisterType<UserIdentityValidator>().As<IIdentityValidator<IdentityUser>>();
            
    		builder.RegisterAssemblyTypes(typeof(UserValidator).Assembly)
				.Where(t => t.Name.EndsWith("Validator"))
				.AsImplementedInterfaces().InstancePerRequest();

			builder.RegisterAssemblyTypes(typeof(UserRepository).Assembly)
				.Where(t => t.Name.EndsWith("Repository"))
				.AsImplementedInterfaces().InstancePerRequest();
    		
        }
    }
}
