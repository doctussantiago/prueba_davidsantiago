﻿using App.Infrastructure.EntityFramework;
using App.Infrastructure.Model.Model;
using App.Infrastructure.Repository.Core;
using App.Infrastructure.Repository.EntityFramework.Users;
using Autofac;
using Autofac.Core;
using Doct.EFLogStorage;
using System.Data.Entity;

namespace App.Infrastructure
{
	public class AutofacConfig
	{
		public static void RegisterTypes(ContainerBuilder builder)
		{
			builder.RegisterType<Entities>().As<DbContext>()
			.Named("entities", typeof(DbContext)).InstancePerRequest();

			builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().WithParameters(new[] {
			new ResolvedParameter((p,c) => p.Name == "context",(p,c) => c.ResolveNamed<DbContext>("entities"))
			});

			builder.RegisterAssemblyTypes(typeof(UserRepository).Assembly)
				.Where(t => t.Name.EndsWith("Repository"))
				.AsImplementedInterfaces().InstancePerRequest();
		}
	}
}
