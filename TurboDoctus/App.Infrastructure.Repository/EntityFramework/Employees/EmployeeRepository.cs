﻿using App.Common.Classes.Validation.Fluent;
using App.Common.Localization;
using App.Infrastructure.Model.Model;
using App.Infrastructure.Repository.Core.Employees;
using doct.Localization;
using doctFramework.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infrastructure.Repository.EntityFramework.Employees
{


    public class EmployeeRepository : BaseEFRepository<Employee>, IEmployeRepository
    {
        public EmployeeRepository(Entities dataContext) : base(dataContext)
        {

        }

        public EmployeeRepository(Entities dataContext, IRepositoryValidator<Activity> validator) : base(dataContext, validator)
        {

        }
    }

    public class EmployeeValidator : BaseRepositoryValidator<Employee>
    {
        private ILocalizationManager _localizator;

        public ILocalizationManager Localization
        {
            get
            {
                return _localizator ?? (_localizator = new LocalizationManager());
            }
        }

        public override void LoadPreInsertRules()
        {
            //throw new NotImplementedException();
        }

        public override void LoadPostInsertRules()
        {
            //throw new NotImplementedException();
        }

        public override void LoadPreUpdateRules()
        {
            //throw new NotImplementedException();
        }

        public override void LoadPostUpdateRules()
        {
            //throw new NotImplementedException();
        }

        public override void LoadPreDeleteRules()
        {
            //throw new NotImplementedException();
        }

        public override void LoadPostDeleteRules()
        {
            //throw new NotImplementedException();
        }
    }


}
