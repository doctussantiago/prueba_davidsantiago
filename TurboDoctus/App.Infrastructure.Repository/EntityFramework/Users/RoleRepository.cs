﻿using App.Infrastructure.Model.Model;
using App.Infrastructure.Model.SecurityEntities;
using App.Infrastructure.Repository.Core.Users;
using AutoMapper;
using doctFramework.Log;
using doctFramework.Validation;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace App.Infrastructure.Repository.EntityFramework.Users
{
	public class RoleRepository : BaseEFRepository<Role>, IRoleRepository
	{

		public RoleRepository(Entities entities, IRepositoryValidator<Role> validator)
			: base(entities, validator)
		{
		}

		public RoleRepository(Entities entities) : base(entities) { }

		public Task CreateAsync(IdentityRole role)
		{
			throw new System.NotImplementedException();
		}

		public Task DeleteAsync(IdentityRole role)
		{
			throw new System.NotImplementedException();
		}

		public async Task<IdentityRole> FindByIdAsync(int roleId)
		{
			var role = await _dbSet.FirstOrDefaultAsync(p => p.RoleId == roleId);
			return Mapper.Map<IdentityRole>(role);
		}

		async Task<IdentityRole> Microsoft.AspNet.Identity.IRoleStore<IdentityRole, int>.FindByNameAsync(string roleName)
		{
			var role = await _dbSet.FirstOrDefaultAsync(p => p.Name == roleName);
			return Mapper.Map<IdentityRole>(role);
		}

		public Task UpdateAsync(IdentityRole role)
		{
			throw new System.NotImplementedException();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Disposes all external resources.
		/// </summary>
		/// <param name="disposing">The dispose indicator.</param>
		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (_entities != null)
				{
					_entities.Dispose();
					_entities = null;
				}
			}
		}

		public IQueryable<IdentityRole> Roles
		{
			get { return _dbSet.ToList().Select(p => Mapper.DynamicMap<IdentityRole>(p)).AsQueryable(); }
		}
	}
}