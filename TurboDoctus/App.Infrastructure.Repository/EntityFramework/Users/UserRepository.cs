﻿using App.Infrastructure.Model.Model;
using App.Infrastructure.Model.SecurityEntities;
using App.Infrastructure.Repository.Core.Users;
using AutoMapper;
using doctFramework.Log;
using doctFramework.Validation;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using App.Common.Classes.Validation.Fluent;
using doct.Localization;
using App.Common.Localization;
using FluentValidation;

namespace App.Infrastructure.Repository.EntityFramework.Users
{
	public class UserRepository : BaseEFRepository<User>, IUserRepository
	{

		public UserRepository(Entities entities)
			: base(entities)
		{
		}

		public UserRepository(Entities dataContext, IRepositoryValidator<User> validator)
			: base(dataContext, validator)
		{

		}


		#region IUserStore<User, int> Members
		public async Task CreateAsync(IdentityUser user)
		{
			user.IsBlocked = false;
			base.Insert(Mapper.Map<User>(user));
			await _entities.SaveChangesAsync();
		}

		public async Task DeleteAsync(IdentityUser user)
		{
			var u = await _dbSet.FindAsync(user.UserId);
			base.Delete(u);
			await _entities.SaveChangesAsync();
		}

		public async Task<IdentityUser> FindByIdAsync(int userId)
		{
			var u = await _dbSet.FirstOrDefaultAsync(p => p.UserId == userId);
			return Mapper.Map<IdentityUser>(u);
		}

		public async Task<IdentityUser> FindByNameAsync(string userName)
		{
			var u = await _dbSet.FirstOrDefaultAsync(p => p.UserName == userName);
			return Mapper.Map<IdentityUser>(u);

		}

		public async Task UpdateAsync(IdentityUser user)
		{
			var oldUser = await _dbSet.FindAsync(user.Id);
			bool changed = false;
			base.Update(Mapper.Map<User>(user), oldUser, out changed);
			await _entities.SaveChangesAsync();
		}
		#endregion

		#region IDisposable Members
		public void Dispose()
		{
			// Dispose does nothing since we want Unity to manage the lifecycle of our Unit of Work
		}
		#endregion

		#region IUserClaimStore<User, int> Members
		public async Task AddClaimAsync(IdentityUser user, System.Security.Claims.Claim claim)
		{
			if (user == null)
				throw new ArgumentNullException("user");
			if (claim == null)
				throw new ArgumentNullException("claim");

			var u = await _dbSet.FindAsync(user.Id);
			if (u == null)
				throw new ArgumentException("User does not correspond to a User entity.", "user");

			var c = new Claim()
			{
				ClaimType = claim.Type,
				ClaimValue = claim.Value,
				User = u
			};
			c.User = u;
			u.Claim.Add(c);

			await _entities.SaveChangesAsync();
		}

		Task<IList<System.Security.Claims.Claim>> IUserClaimStore<IdentityUser, int>.GetClaimsAsync(IdentityUser user)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			var claims = _entities.Set<Claim>().Where(p => p.UserId == user.Id).ToList();
			return Task.FromResult<IList<System.Security.Claims.Claim>>(claims.Select(x => new System.Security.Claims.Claim(x.ClaimType, x.ClaimValue)).ToList());
		}

		public async Task RemoveClaimAsync(IdentityUser user, System.Security.Claims.Claim claim)
		{
			if (user == null)
				throw new ArgumentNullException("user");
			if (claim == null)
				throw new ArgumentNullException("claim");

			var u = await _dbSet.FindAsync(user.Id);
			if (u == null)
				throw new ArgumentException("User does not correspond to a User entity.", "user");

			var c = u.Claim.FirstOrDefault(x => x.ClaimType == claim.Type && x.ClaimValue == claim.Value);
			u.Claim.Remove(c);

			await _entities.SaveChangesAsync();
		}

		#endregion

		#region IUserLoginStore<User, int> Members
		public async Task AddLoginAsync(IdentityUser user, UserLoginInfo login)
		{
			if (user == null)
				throw new ArgumentNullException("user");
			if (login == null)
				throw new ArgumentNullException("login");

			var u = await _dbSet.FindAsync(user.Id);
			if (u == null)
				throw new ArgumentException("User does not correspond to a User entity.", "user");

			var l = new ExternalLogin
			{
				LoginProvider = login.LoginProvider,
				ProviderKey = login.ProviderKey,
				User = u
			};
			u.ExternalLogin.Add(l);
			await _entities.SaveChangesAsync();
		}

		public async Task<IdentityUser> FindAsync(UserLoginInfo login)
		{
			if (login == null)
				throw new ArgumentNullException("login");

			var identityUser = default(User);

			var l = await _entities.Set<ExternalLogin>().FirstOrDefaultAsync(p => p.LoginProvider == login.LoginProvider && p.ProviderKey == login.ProviderKey);
			if (l != null)
				identityUser = l.User;

			return Mapper.Map<IdentityUser>(identityUser);
		}

		public Task<IList<UserLoginInfo>> GetLoginsAsync(IdentityUser user)
		{
			if (user == null)
				throw new ArgumentNullException("user");


			return Task.FromResult<IList<UserLoginInfo>>(_entities.Set<ExternalLogin>().Where(p => p.UserId == user.Id).ToList().Select(x => new UserLoginInfo(x.LoginProvider, x.ProviderKey)).ToList());
		}

		public async Task RemoveLoginAsync(IdentityUser user, UserLoginInfo login)
		{
			if (user == null)
				throw new ArgumentNullException("user");
			if (login == null)
				throw new ArgumentNullException("login");

			var u = await _dbSet.FindAsync(user.Id);
			if (u == null)
				throw new ArgumentException("User does not correspond to a User entity.", "user");

			var l = u.ExternalLogin.FirstOrDefault(x => x.LoginProvider == login.LoginProvider && x.ProviderKey == login.ProviderKey);
			u.ExternalLogin.Remove(l);
			await _entities.SaveChangesAsync();
		}
		#endregion

		#region IUserRoleStore<User, int> Members
		public async Task AddToRoleAsync(IdentityUser user, string roleName)
		{
			if (user == null)
				throw new ArgumentNullException("user");
			if (string.IsNullOrWhiteSpace(roleName))
				throw new ArgumentException("Argument cannot be null, empty, or whitespace: roleName.");

			var u = await _dbSet.FindAsync(user.Id);
			if (u == null)
				throw new ArgumentException("User does not correspond to a User entity.", "user");
			var r = _entities.Set<Role>().FirstOrDefault(p => p.Name == roleName);
			if (r == null)
				throw new ArgumentException("roleName does not correspond to a Role entity.", "roleName");

			u.Role.Add(r);
			await _entities.SaveChangesAsync();
		}

		public async Task<IList<string>> GetRolesAsync(IdentityUser user)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			var u = await _dbSet.FindAsync(user.Id);

			return u.Role.Select(x => x.Name).ToList();
		}

		public async Task<bool> IsInRoleAsync(IdentityUser user, string roleName)
		{
			if (user == null)
				throw new ArgumentNullException("user");
			if (string.IsNullOrWhiteSpace(roleName))
				throw new ArgumentException("Argument cannot be null, empty, or whitespace: role.");

			var u = await _dbSet.FindAsync(user.Id);
			if (u == null)
				throw new ArgumentException("User does not correspond to a User entity.", "user");

			return u.Role.Any(x => x.Name == roleName);
		}

		public async Task RemoveFromRoleAsync(IdentityUser user, string roleName)
		{
			if (user == null)
				throw new ArgumentNullException("user");
			if (string.IsNullOrWhiteSpace(roleName))
				throw new ArgumentException("Argument cannot be null, empty, or whitespace: role.");

			var u = await _dbSet.FindAsync(user.Id);
			if (u == null)
				throw new ArgumentException("User does not correspond to a User entity.", "user");

			var r = u.Role.FirstOrDefault(x => x.Name == roleName);
			u.Role.Remove(r);

			await _entities.SaveChangesAsync();
		}
		#endregion

		#region IUserPasswordStore<User, int> Members
		public async Task<string> GetPasswordHashAsync(IdentityUser user)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			var u = await _dbSet.FindAsync(user.Id);
			return u.PasswordHash;
		}

		public async Task<bool> HasPasswordAsync(IdentityUser user)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			var u = await _dbSet.FindAsync(user.Id);

			return !(string.IsNullOrWhiteSpace(u.PasswordHash));
		}

		public async Task SetPasswordHashAsync(IdentityUser user, string passwordHash)
		{
			await Task.Run(() => { user.PasswordHash = passwordHash; });

		}
		#endregion

		#region IUserSecurityStampStore<User, int> Members
		public async Task<string> GetSecurityStampAsync(IdentityUser user)
		{
			if (user == null)
				throw new ArgumentNullException("user");
			var u = await _dbSet.FindAsync(user.Id);
			return u.SecurityStamp;
		}

		public async Task SetSecurityStampAsync(IdentityUser user, string stamp)
		{
			await Task.Run(() => { user.SecurityStamp = stamp; });
		}
		#endregion


		public IQueryable<IdentityUser> Users
		{
			get { return _dbSet.ToList().Select(p => Mapper.Map<IdentityUser>(p)).AsQueryable(); }
		}

	}

	public class UserValidator : BaseRepositoryValidator<User>
	{
		private ILocalizationManager _localizator;

		public ILocalizationManager Localizator
		{
			get
			{
				return _localizator ?? (_localizator = new LocalizationManager());
			}

		}

		public override void LoadPreInsertRules()
		{
			PreInsertValidator.RuleFor(e => e.FullName).NotEmpty().WithMessage(Localizator.GetStringResourceWithFormat("User", "FullName_Field_Not_Null"));
			PreInsertValidator.RuleFor(e => e.FullName).Length(1, 250).WithMessage(Localizator.GetStringResourceWithFormat("User", "FullName_Max_Length"));
			PreInsertValidator.RuleFor(e => e.UserName).NotEmpty().WithMessage(Localizator.GetStringResourceWithFormat("User", "UserName_Field_Not_Null"));
			PreInsertValidator.RuleFor(e => e.UserName).Length(1, 256).WithMessage(Localizator.GetStringResourceWithFormat("User", "UserName_Max_Length"));
			PreInsertValidator.RuleFor(e => e.PasswordHash).NotEmpty().WithMessage(Localizator.GetStringResourceWithFormat("User", "Password_Field_Not_Null"));

		}

		public override void LoadPostInsertRules()
		{
		}

		public override void LoadPreUpdateRules()
		{
			PreUpdateValidator.RuleFor(e => e.FullName).NotEmpty().WithMessage(Localizator.GetStringResourceWithFormat("User", "FullName_Field_Not_Null"));
			PreUpdateValidator.RuleFor(e => e.FullName).Length(1, 250).WithMessage(Localizator.GetStringResourceWithFormat("User", "FullName_Max_Length"));
			PreUpdateValidator.RuleFor(e => e.UserName).NotEmpty().WithMessage(Localizator.GetStringResourceWithFormat("User", "UserName_Field_Not_Null"));
			PreUpdateValidator.RuleFor(e => e.UserName).Length(1, 256).WithMessage(Localizator.GetStringResourceWithFormat("User", "UserName_Max_Length"));
		}

		public override void LoadPostUpdateRules()
		{
		}

		public override void LoadPreDeleteRules()
		{
		}

		public override void LoadPostDeleteRules()
		{
		}
	}

	public class PasswordIdentityValidator : IIdentityValidator<string>
	{
		private ILocalizationManager _localizator;

		public ILocalizationManager Localizator
		{
			get
			{
				return _localizator ?? (_localizator = new LocalizationManager());
			}

		}

		public Task<IdentityResult> ValidateAsync(string item)
		{
			if (string.IsNullOrEmpty(item))
			{
				return Task.FromResult(IdentityResult.Failed(Localizator.GetStringResourceWithFormat("User", "Password_Field_Not_Null")));
			}

			if (item.Length < 6)
			{
				return Task.FromResult(IdentityResult.Failed(Localizator.GetStringResourceWithFormat("User", "Password_Max_Length")));
			}

			return Task.FromResult(IdentityResult.Success);
		}
	}

	public class UserIdentityValidator : IIdentityValidator<IdentityUser>
	{
		private ILocalizationManager _localizator;

		public ILocalizationManager Localizator
		{
			get
			{
				return _localizator ?? (_localizator = new LocalizationManager());
			}

		}

		public Task<IdentityResult> ValidateAsync(IdentityUser item)
		{
			if (string.IsNullOrEmpty(item.UserName))
			{
				return Task.FromResult(IdentityResult.Failed(Localizator.GetStringResourceWithFormat("User", "UserName_Field_Not_Null")));

			}

			if (item.UserName.Length > 256)
			{
				return Task.FromResult(IdentityResult.Failed(Localizator.GetStringResourceWithFormat("User", "UserName_Max_Length")));
			}

			return Task.FromResult(IdentityResult.Success);
		}
	}

}