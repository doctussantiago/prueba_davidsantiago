﻿using App.Infrastructure.Model.Model;
using doctFramework.Log;
using doctFramework.Repository;
using doctFramework.Validation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace App.Infrastructure.Repository.EntityFramework
{
	public abstract class BaseEFRepository<TEntity> : IRepository<TEntity> where TEntity : class, new()
	{

		private IRepositoryValidator<TEntity> _validator = null;

		protected Entities _entities;
		protected DbSet<TEntity> _dbSet;

		public BaseEFRepository(Entities entities)
		{
			this._entities = entities;
			this._dbSet = entities.Set<TEntity>();
		}

		public BaseEFRepository(Entities entities, IRepositoryValidator<TEntity> validator)
		{
			this._entities = entities;
			this._dbSet = entities.Set<TEntity>();
			this._validator = validator;
		}

		public IQueryable<TEntity> All
		{
			get { return _dbSet; }
		}

		public virtual TEntity Insert(TEntity entity)
		{

			if (_validator != null)
			{
				_validator.ValidatePreInsert(entity);
			}

			_dbSet.Add(entity);

			return entity;

		}

		public virtual TEntity Delete(TEntity entity)
		{
			if (_validator != null)
			{
				_validator.ValidatePreDelete(entity);
			}

			_dbSet.Remove(entity);

			return entity;

		}


		public virtual TEntity Update(TEntity editedEntity, TEntity originalEntity, out bool changed)
		{

			_entities.Entry(originalEntity).CurrentValues.SetValues(editedEntity);
			if (_validator != null)
			{
				_validator.ValidatePreUpdate(originalEntity);
			}

			changed = _entities.Entry(originalEntity).State == System.Data.Entity.EntityState.Modified;

			return originalEntity;

		}

		public virtual TEntity FindById(object id)
		{
			var newId = CastPrimaryKey(id);
			return _dbSet.Find(newId);
		}


		#region FUNCTIONS
		protected object GetPrimaryKeyValue(TEntity entity)
		{

			ObjectContext objectContext = ((IObjectContextAdapter)_entities).ObjectContext;
			ObjectSet<TEntity> set = objectContext.CreateObjectSet<TEntity>();
			IEnumerable<string> keyNames = set.EntitySet.ElementType.KeyMembers.Select(k => k.Name);
			string keyName = keyNames.FirstOrDefault();

			if (keyNames.Count() > 1)
			{
				throw new ApplicationException("El objeto tiene mas de 1 llave primaria. Lo cual no permite la automatización de BaseModel en el Update");
			}

			if (keyName == null)
			{
				throw new ApplicationException("El objeto no tiene llave primaria. Lo cual no permite la automatización de BaseModel en el Update");
			}

			return entity.GetType().GetProperty(keyName).GetValue(entity, null); ;

		}

		protected string GetPrimaryKeyName()
		{

			ObjectContext objectContext = ((IObjectContextAdapter)_entities).ObjectContext;
			ObjectSet<TEntity> set = objectContext.CreateObjectSet<TEntity>();
			IEnumerable<string> keyNames = set.EntitySet.ElementType.KeyMembers.Select(k => k.Name);
			string keyName = keyNames.FirstOrDefault();

			if (keyNames.Count() > 1)
			{
				throw new ApplicationException("El objeto tiene mas de 1 llave primaria. Lo cual no permite la automatización de BaseModel en el Update");
			}

			if (keyName == null)
			{
				throw new ApplicationException("El objeto no tiene llave primaria. Lo cual no permite la automatización de BaseModel en el Update");
			}

			return keyName;

		}

		protected object CastPrimaryKey(object id)
		{
			string keyName = GetPrimaryKeyName();
			Type keyType = typeof(TEntity).GetProperty(keyName).PropertyType;
			return Convert.ChangeType(id, keyType);
		}

		#endregion

		#region SAVE

		//public virtual void SaveChanges()
		//{

		//	this._dataContext.SaveChanges();
		//}

		#endregion

	}

	public abstract class BaseVwEFRepository<TEntity, TvwEntity> : BaseEFRepository<TEntity>, IVwRepository<TvwEntity>
		where TvwEntity : class, new()
		where TEntity : class, new()
	{
		protected DbSet<TvwEntity> _dbVwSet;

		public BaseVwEFRepository(Entities entities)
			: base(entities)
		{
			_dbVwSet = entities.Set<TvwEntity>();
		}

		public BaseVwEFRepository(Entities entities, IRepositoryValidator<TEntity> validator)
			: base(entities, validator)
		{
			_dbVwSet = entities.Set<TvwEntity>();
		}

		#region GET

		public virtual IQueryable<TvwEntity> GetVwAll()
		{
			return _dbVwSet;
		}
		public virtual TvwEntity GetVwById(object id)
		{
			return _dbVwSet.Find(id);

		}

		#endregion



	}
}
