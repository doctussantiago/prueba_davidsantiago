﻿using App.Common.Classes.Validation.Fluent;
using App.Common.Localization;
using App.Infrastructure.Model.Model;
using App.Infrastructure.Repository.Core.WorkingHourEs;
using doct.Localization;
using doctFramework.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Infrastructure.Repository.EntityFramework.WorkingHourEs
{
    public class WorkingHourRepository : BaseEFRepository<WorkingHour>, IWorkingHourRepository
    {
        public WorkingHourRepository(Entities dataContext) : base(dataContext)
        {

        }

        public WorkingHourRepository(Entities dataContext, IRepositoryValidator<WorkingHour> validator) : base(dataContext, validator)
        {

        }
    }


    public class WorkingHourValidator : BaseRepositoryValidator<WorkingHour>
    {
        private ILocalizationManager _localizator;

        public ILocalizationManager Localization
        {
            get
            {
                return _localizator ?? (_localizator = new LocalizationManager());
            }
        }

        public override void LoadPreInsertRules()
        {
            //throw new NotImplementedException();
        }

        public override void LoadPostInsertRules()
        {
            //throw new NotImplementedException();
        }

        public override void LoadPreUpdateRules()
        {
            //throw new NotImplementedException();
        }

        public override void LoadPostUpdateRules()
        {
            //throw new NotImplementedException();
        }

        public override void LoadPreDeleteRules()
        {
            //throw new NotImplementedException();
        }

        public override void LoadPostDeleteRules()
        {
            //throw new NotImplementedException();
        }
    }


}
