﻿
using App.Infrastructure.Model;
using App.Infrastructure.Model.Model;
using App.Infrastructure.Model.SecurityEntities;
using AutoMapper;

namespace App.Infrastructure.Repository
{
	public class AutomapperConfig
	{

		public static void CreateMaps()
		{

			Mapper.CreateMap<User, IdentityUser>().ReverseMap();
			Mapper.CreateMap<Role, IdentityRole>().ReverseMap();
			Mapper.CreateMap<Claim, System.Security.Claims.Claim>().ReverseMap();

		}
	}
}
