﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Infrastructure.Model;

namespace App.Domain.Base
{
	public interface IBaseService<TDTO>
		where TDTO : class
	{

		IEnumerable<TDTO> GetAll();
		void Create(TDTO dto);
		void Delete(object id);
		void Update(object id, TDTO dto);
		Task CreateAsync(TDTO dto);
		Task DeleteAsync(object id);
		Task UpdateAsync(object id, TDTO dto);
		TDTO FindById(object Id);
		object ExecuteKendoResult(object request);


	}


	public interface IVwBaseService<TVwDTO>
		where TVwDTO : class
	{

		IEnumerable<TVwDTO> GetVwAll();

		object ExecuteVwKendoResult(object request);


	}
}
