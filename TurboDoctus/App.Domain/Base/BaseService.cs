﻿using App.Common;
using AutoMapper;
using doct.Cache;
using doctFramework.Repository;
using System.Collections.Generic;
using System.Linq;
using App.Common.Classes.Validation;
using doctFramework.Log;
using System;
using doctFramework.Validation;
using App.Infrastructure.Repository.Core;
using System.Threading.Tasks;
using App.Common.Security;

namespace App.Domain.Base
{
	public abstract class BaseService<TDTO, TEntity> : IBaseService<TDTO>
		where TDTO : class
		where TEntity : class
	{
		protected KendoResult _kendoResult;
		protected IRepository<TEntity> _repository;
		protected IUnitOfWork _unitOfWork;
		protected ICacheStorage _cacheStorage;
		protected ILog _log;
		protected IUserSession _userSession;

		public string _cacheKey = string.Format("list_{0}", typeof(TEntity).Name);


		public KendoResult KendoResult
		{
			get
			{
				return _kendoResult ?? (_kendoResult = new KendoResult());
			}

		}

		public BaseService() { }

		public BaseService(IUnitOfWork unitOfWork, IRepository<TEntity> repository, ICacheStorage cacheStorage)
		{
			this._unitOfWork = unitOfWork;
			this._repository = repository;
			this._cacheStorage = cacheStorage;
		}

		public BaseService(IUnitOfWork unitOfWork, IRepository<TEntity> repository, ICacheStorage cacheStorage, ILog log, IUserSession userSession)
		{
			this._unitOfWork = unitOfWork;
			this._repository = repository;
			this._cacheStorage = cacheStorage;
			this._log = log;
			this._userSession = userSession;
		}

		public virtual void Create(TDTO dto)
		{
			TEntity entity = Mapper.Map<TEntity>(dto);
			entity = _repository.Insert(entity);
			_cacheStorage.Clear(_cacheKey);
			_unitOfWork.Commit();

			if (_log != null && _userSession != null)
			{
				_log.InsertLog(doct.Log.Model.LogOperationTypes.Insert, DateTime.Now, entity, null, typeof(TEntity).FullName, username: _userSession.UserName, ip: _userSession.IP);
			}

		}

		public virtual void Delete(object id)
		{
			TEntity entity = _repository.FindById(id);
			_repository.Delete(entity);
			_unitOfWork.Commit();
			_cacheStorage.Clear(_cacheKey);
			if (_log != null && _userSession != null)
			{
				_log.InsertLog(doct.Log.Model.LogOperationTypes.Delete, DateTime.Now, null, entity, typeof(TEntity).FullName, username: _userSession.UserName, ip: _userSession.IP);
			}
		}

		public virtual void Update(object id, TDTO dto)
		{
			TEntity entity = Mapper.Map<TEntity>(dto);
			TEntity oldEntity = _repository.FindById(id);
			bool changed = false;
			entity = _repository.Update(entity, oldEntity, out changed);
			_unitOfWork.Commit();
			_cacheStorage.Clear(_cacheKey);
			if ((_log != null) && (_userSession != null) && (changed))
			{
				_log.InsertLog(doct.Log.Model.LogOperationTypes.Update, DateTime.Now, entity, oldEntity, typeof(TEntity).FullName);
			}
		}

		public async virtual Task CreateAsync(TDTO dto)
		{
			TEntity entity = Mapper.Map<TEntity>(dto);
			entity = _repository.Insert(entity);
			_cacheStorage.Clear(_cacheKey);
			await _unitOfWork.CommitAsync();

			if (_log != null && _userSession != null)
			{
				_log.InsertLog(doct.Log.Model.LogOperationTypes.Insert, DateTime.Now, entity, null, typeof(TEntity).FullName, username: _userSession.UserName, ip: _userSession.IP);
			}

		}

		public async virtual Task DeleteAsync(object id)
		{
			TEntity entity = _repository.FindById(id);
			_repository.Delete(entity);
			await _unitOfWork.CommitAsync();
			_cacheStorage.Clear(_cacheKey);
			if (_log != null && _userSession != null)
			{
				_log.InsertLog(doct.Log.Model.LogOperationTypes.Delete, DateTime.Now, null, entity, typeof(TEntity).FullName, username: _userSession.UserName, ip: _userSession.IP);
			}
		}

		public async virtual Task UpdateAsync(object id, TDTO dto)
		{
			TEntity entity = Mapper.Map<TEntity>(dto);
			TEntity oldEntity = _repository.FindById(id);
			bool changed = false;
			entity = _repository.Update(entity, oldEntity, out changed);
			await _unitOfWork.CommitAsync();
			_cacheStorage.Clear(_cacheKey);
			if ((_log != null) && (_userSession != null) && (changed))
			{
				_log.InsertLog(doct.Log.Model.LogOperationTypes.Update, DateTime.Now, entity, oldEntity, typeof(TEntity).FullName, username: _userSession.UserName, ip: _userSession.IP);
			}
		}

		public virtual TDTO FindById(object Id)
		{
			return Mapper.Map<TDTO>(_repository.FindById(Id));
		}

		public virtual object ExecuteKendoResult(object request)
		{
			return KendoResult.Result<TEntity, TDTO>(_repository.All, request);

		}

		public virtual IEnumerable<TDTO> GetAll()
		{
			List<TEntity> list;

			list = _cacheStorage.Get<List<TEntity>>(_cacheKey);
			if (list == null)
			{
				list = _repository.All.ToList();
				_cacheStorage.Insert(_cacheKey, list);
			}
			return list.Select(data => Mapper.DynamicMap<TDTO>(data));
		}

		/// <summary>
		/// Permite copiar la entidad sin que quede vinculada al contexto y preserve los datos asi se haga el SaveChanges
		/// </summary>
		/// <param name="originalEntity">Entidad con los datos originales</param>
		/// <param name="oEntity">Entidad que se le asignaran los valores de la original</param>
		private static void CopyEntityToSendLog(TEntity originalEntity, TEntity oEntity)
		{
			foreach (var pS in originalEntity.GetType().GetProperties())
			{
				foreach (var pT in oEntity.GetType().GetProperties())
				{
					if (pT.Name != pS.Name) continue;
					(pT.GetSetMethod()).Invoke(oEntity, new object[] { pS.GetGetMethod().Invoke(originalEntity, null) });
				}
			};
		}
	}

	public abstract class BaseVwService<TDTO, TEntity, TvwDTO, TvwEntity> : BaseService<TDTO, TEntity>, IVwBaseService<TvwDTO>
		where TDTO : class
		where TEntity : class
		where TvwEntity : class
		where TvwDTO : class
	{
		private IVwRepository<TvwEntity> _vwRepository;

		public BaseVwService(IVwRepository<TvwEntity> vwRepository)
			: base()
		{
			_vwRepository = vwRepository;
		}

		public BaseVwService(IUnitOfWork unitOfWork, IRepository<TEntity> repository, ICacheStorage cacheStorage, IVwRepository<TvwEntity> vwRepository)
			: base(unitOfWork, repository, cacheStorage)
		{
			_vwRepository = vwRepository;
		}

		public BaseVwService(IUnitOfWork unitOfWork, IRepository<TEntity> repository, ICacheStorage cacheStorage, ILog log, IUserSession userSession, IVwRepository<TvwEntity> vwRepository)
			: base(unitOfWork, repository, cacheStorage, log, userSession)
		{
			_vwRepository = vwRepository;
		}


		public virtual IEnumerable<TvwDTO> GetVwAll()
		{
			List<TvwEntity> list;

			list = _vwRepository.GetVwAll().ToList();

			return list.Select(data => Mapper.DynamicMap<TvwDTO>(data));
		}

		public virtual object ExecuteVwKendoResult(object request)
		{
			return KendoResult.Result<TvwEntity, TvwDTO>(_vwRepository.GetVwAll(), request);
		}
	}
}





