﻿using App.Domain.Base;
using App.Domain.DTO.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.Services.Employees
{
    public interface IEmployeeService : IBaseService<EmployeeDTO>
    {
        void CreateEmployee(EmployeeDTO DTO);
        void UpdateEmployee(EmployeeDTO DTO);
        void DeleteEmployee(EmployeeDTO DTO);
        void SearchEmployee(EmployeeDTO DTO);
    }
}




