﻿using App.Common.Security;
using App.Domain.Base;
using App.Domain.DTO.Employees;
using App.Infrastructure.Model.Model;
using App.Infrastructure.Repository.Core;
using App.Infrastructure.Repository.Core.Employees;
using AutoMapper;
using doct.Cache;
using doctFramework.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.Services.Employees
{
    public class EmployeeService : BaseService<EmployeeDTO, Employee>, IEmployeeService
    {
        public EmployeeService(IUnitOfWork unitOfWork, ICacheStorage cacheStorage, ILog log, IUserSession userSession, IEmployeRepository repository)
            : base(unitOfWork, repository, cacheStorage, log, userSession)
        {

        }

        public EmployeeService(IUnitOfWork unitOfWork, ICacheStorage cacheStorage, IEmployeRepository repository)
            : base(unitOfWork, repository, cacheStorage)
        {

        }


        public void CreateEmployee(EmployeeDTO DTO)
        {
            Employee entiti = Mapper.Map<Employee>(DTO);
            _repository.Insert(entiti);
        }

        public void UpdateEmployee(EmployeeDTO DTO)
        {
            Employee entiti = Mapper.Map<Employee>(DTO);
            //_repository.Update(entiti);
        }

        public void DeleteEmployee(EmployeeDTO DTO)
        {
            Employee entiti = Mapper.Map<Employee>(DTO);
            _repository.Delete(entiti);
        }

        public void SearchEmployee(EmployeeDTO DTO)
        {
            Employee entiti = Mapper.Map<Employee>(DTO);
            //_repository.All(entiti);
        }
    }
}

