﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Infrastructure.Model.Model;
using Microsoft.AspNet.Identity;
using App.Domain.DTO.Users;
using System.Security.Claims;
using App.Domain.Base;

namespace App.Domain.Services.Users
{
	public interface IRoleService
	{
		List<RoleDTO> GetAll();
	}
}
