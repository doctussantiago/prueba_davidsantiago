﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Infrastructure.Model.Model;
using Microsoft.AspNet.Identity;
using App.Domain.DTO.Users;
using System.Security.Claims;
using App.Domain.Base;

namespace App.Domain.Services.Users
{
	public interface IUserService
	{
		Task<ClaimsIdentity> GetClaimsFromUserIfExistAsync(LoginDTO user, string authenticationType);
		Task<bool> HasLocalPasswordAsync(int userId);
		Task ChangePasswordAsync(ChangePasswordDTO user, bool hasLocalPassword);
		Task<IList<UserLoginInfo>> GetLoginsAsync(int userId);
		Task<UserDTO> FindByIdAsync(object id);

		IEnumerable<UserDTO> GetAll();
		Task CreateAsync(UserDTO dto);
		Task DeleteAsync(object id);
		Task UpdateAsync(object id, UserDTO dto);

		object ExecuteKendoResult(object request);
	}
}
