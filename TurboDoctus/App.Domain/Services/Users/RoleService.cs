﻿using App.Domain.DTO.Users;
using App.Infrastructure.Model.SecurityEntities;
using App.Infrastructure.Repository.Core.Users;
using App.Infrastructure.Repository.EntityFramework.Users;
using AutoMapper;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;

namespace App.Domain.Services.Users
{
	public class RoleService : RoleManager<IdentityRole, int>, IRoleService
	{

		public RoleService(IRoleRepository repository) : base(repository) { }
		public List<RoleDTO> GetAll()
		{
			return base.Roles.ToList().Select(p => Mapper.Map<RoleDTO>(p)).ToList();
		}
	}

}
