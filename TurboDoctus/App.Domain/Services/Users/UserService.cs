﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using App.Common;
using App.Domain.DTO.Users;
using App.Infrastructure.Model.SecurityEntities;
using App.Infrastructure.Repository.Core.Users;
using AutoMapper;
using doct.Cache;
using doct.Exception;
using doctFramework.Log;
using Microsoft.AspNet.Identity;

namespace App.Domain.Services.Users
{

	public class UserService : UserManager<IdentityUser, int>, IUserService
	{
		private IRoleRepository _roleRepository;
		private ICacheStorage _cacheStorage;
		private ILog _log;

		public string _cacheKeyUsers = string.Format("list_{0}", typeof(IdentityUser).Name);
		public string _cacheKeyRoles = string.Format("list_{0}", typeof(IdentityRole).Name);

		public UserService(IUserRepository repository, IRoleRepository roleRepository, ICacheStorage cacheStorage, IIdentityValidator<string> passwordValidator, IIdentityValidator<IdentityUser> userValidator)
			: base(repository)
		{
			_roleRepository = roleRepository;
			_cacheStorage = cacheStorage;
			base.PasswordValidator = passwordValidator;
			base.UserValidator = userValidator;
		}

		public UserService(IUserRepository repository, IRoleRepository roleRepository, ICacheStorage cacheStorage, ILog log, IIdentityValidator<string> passwordValidator, IIdentityValidator<IdentityUser> userValidator)
			: base(repository)
		{
			_roleRepository = roleRepository;
			_cacheStorage = cacheStorage;
			_log = log;
			base.PasswordValidator = passwordValidator;
			base.UserValidator = userValidator;
		}

		public async Task<ClaimsIdentity> GetClaimsFromUserIfExistAsync(LoginDTO user, string authenticationType)
		{
			var u = await base.FindAsync(user.UserName, user.Password);
			if (u == null)
			{
				List<string> errors = new List<string>();
				errors.Add("Usuario o contraseña incorrectos");
				throw new ValidationRepositoryException(errors);
			}
			var claim = await base.CreateIdentityAsync(u, authenticationType);
			if (_log != null && u != null)
			{
				_log.InsertLog(doct.Log.Model.LogOperationTypes.Login, DateTime.Now, u, null, typeof(IdentityUser).FullName);
			}
			return claim;
		}

		public async Task CreateAsync(UserDTO dto)
		{
			var user = Mapper.Map<IdentityUser>(dto);
			var validatePassword = await base.PasswordValidator.ValidateAsync(dto.Password);
			if (!validatePassword.Succeeded)
			{
				throw new ValidationRepositoryException(validatePassword.Errors.ToList());
			}
			else
			{
				var result = await base.CreateAsync(user, dto.Password);
				IdentityUser userCreated = null;
				if (!result.Succeeded)
				{
					throw new ValidationRepositoryException(result.Errors.ToList());
				}
				else
				{
					userCreated = await base.FindByNameAsync(dto.UserName);
					if (dto.Roles != null)
					{
						var userId = userCreated.Id;
						await base.AddToRolesAsync(userId, dto.Roles.Select(x => x.Name).ToArray());
					}
				}
				if (_log != null && userCreated != null)
				{
					_log.InsertLog(doct.Log.Model.LogOperationTypes.Insert, DateTime.Now, userCreated, null, typeof(IdentityUser).FullName);
				}

			}

		}


		public async Task<bool> HasLocalPasswordAsync(int userId)
		{
			var u = await base.FindByIdAsync(userId);
			if (u != null)
			{
				return u.PasswordHash != null;
			}
			return false;
		}

		public async Task ChangePasswordAsync(ChangePasswordDTO user, bool hasLocalPassword)
		{
			user.Id = user.UserId;
			if (hasLocalPassword)
			{
				IdentityResult result = await base.ChangePasswordAsync(user.UserId, user.OldPassword, user.NewPassword);
				if (!result.Succeeded)
				{
					throw new ValidationRepositoryException(result.Errors.ToList());
				}
			}
			else
			{
				IdentityResult result = await base.AddPasswordAsync(user.UserId, user.NewPassword);
				if (!result.Succeeded)
				{
					throw new ValidationRepositoryException(result.Errors.ToList());
				}
			}
		}


		public async Task<IList<UserLoginInfo>> GetLoginsAsync(int userId)
		{
			return await base.GetLoginsAsync(userId);
		}

		public object ExecuteKendoResult(object request)
		{
			return new KendoResult().Result<IdentityUser, UserDTO>(base.Users, request);
		}

		public async Task UpdateAsync(object id, UserDTO dto)
		{
			var user = Mapper.Map<IdentityUser>(dto);
			var pass = await base.FindByIdAsync(dto.Id);
			user.PasswordHash = pass.PasswordHash;
			user.SecurityStamp = pass.SecurityStamp;
			user.IsBlocked = pass.IsBlocked;
			if (dto.Roles != null)
			{
				var roles = await base.GetRolesAsync(dto.Id);
				await base.RemoveFromRolesAsync(dto.Id, roles.ToArray());
				await base.AddToRolesAsync(dto.Id, dto.Roles.Select(x => x.Name).ToArray());
			}

			await base.UpdateAsync(user);
		}

		public async Task<UserDTO> FindByIdAsync(object Id)
		{
			var u = await base.FindByIdAsync((int)Id);
			var user = Mapper.Map<UserDTO>(u);
			user.Roles = await SelectedRoles((int)Id);
			return user;
		}

		public async Task DeleteAsync(object id)
		{
			var user = await base.FindByIdAsync((int)id);
			user.IsBlocked = !user.IsBlocked;
			await base.UpdateAsync(user);
		}

		private async Task<List<RoleDTO>> SelectedRoles(int id)
		{
			var allRoles = _cacheStorage.Get<List<RoleDTO>>(_cacheKeyRoles);
			if (allRoles == null)
			{
				allRoles = _roleRepository.Roles.ToList().Select(x => Mapper.Map<RoleDTO>(x)).ToList();
				_cacheStorage.Insert(_cacheKeyUsers, allRoles);
			}
			var userRoles = await base.GetRolesAsync(id);
			allRoles.ForEach(z => z.IsSelected = userRoles.Contains(z.Name));
			return allRoles;
		}

		public IEnumerable<UserDTO> GetAll()
		{
			List<UserDTO> list;

			list = _cacheStorage.Get<List<UserDTO>>(_cacheKeyUsers);
			if (list == null)
			{
				list = base.Users.ToList().Select(x => Mapper.Map<UserDTO>(x)).ToList();
				_cacheStorage.Insert(_cacheKeyUsers, list);
			}
			return list.Select(data => Mapper.DynamicMap<UserDTO>(data));
		}


	}


}
