﻿using App.Domain.Base;
using App.Domain.DTO.WorkingHoures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.Services.WorkingHoures
{
    public interface IWorkingHourService : IBaseService<WorkingHourDTO>
    {
        void CreateWorkingHour(WorkingHourDTO DTO);
        void UpdateWorkingHour(WorkingHourDTO DTO);
        void DeleteWorkingHour(WorkingHourDTO DTO);
        void SearchWorkingHour(WorkingHourDTO DTO);
        void ValidateWorkingHour(WorkingHourDTO DTO);
    }
}
