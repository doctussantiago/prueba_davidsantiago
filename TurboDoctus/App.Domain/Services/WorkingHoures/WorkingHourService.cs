﻿using App.Common.Security;
using App.Domain.Base;
using App.Domain.DTO.WorkingHoures;
using App.Infrastructure.Model.Model;
using App.Infrastructure.Repository.Core;
using App.Infrastructure.Repository.Core.WorkingHourEs;
using AutoMapper;
using doct.Cache;
using doctFramework.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.Services.WorkingHoures
{
    public class WorkingHourService : BaseService<WorkingHourDTO, WorkingHour>, IWorkingHourService
    {     
        public WorkingHourService(IUnitOfWork unitOfWork, ICacheStorage cacheStorage, ILog log, IUserSession userSession, IWorkingHourRepository repository)
            : base(unitOfWork, repository, cacheStorage, log, userSession)
        {

        }

        public WorkingHourService(IUnitOfWork unitOfWork, ICacheStorage cacheStorage, IWorkingHourRepository repository)
            : base(unitOfWork, repository, cacheStorage)
        {

        }


        public void CreateWorkingHour(WorkingHourDTO DTO)
        {
            WorkingHour entiti = Mapper.Map<WorkingHour>(DTO);
            _repository.Insert(entiti);
        }

        public void UpdateWorkingHour(WorkingHourDTO DTO)
        {
        WorkingHour entiti = Mapper.Map<WorkingHour>(DTO);
            //_repository.Update(entiti);
        }

        public void DeleteWorkingHour(WorkingHourDTO DTO)
        {
        WorkingHour entiti = Mapper.Map<WorkingHour>(DTO);
            _repository.Delete(entiti);
        }

        public void SearchWorkingHour(WorkingHourDTO DTO)
        {
        WorkingHour entiti = Mapper.Map<WorkingHour>(DTO);
            //_repository.All(entiti);
        }

        public void ValidateWorkingHour(WorkingHourDTO DTO)
        {
            WorkingHour entiti = Mapper.Map<WorkingHour>(DTO);
            //_repository.All(entiti);
        }
    }
}
