﻿using App.Domain.Base;
using App.Domain.DTO.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.Services.Activities
{
    public interface IActivityService : IBaseService<ActivityDTO>
    {
        void CreateActivity(ActivityDTO DTO);
        void UpdateActivity(ActivityDTO DTO);
        void DeleteActivity(ActivityDTO DTO);
        void searchActivity(ActivityDTO DTO);
    }
}

