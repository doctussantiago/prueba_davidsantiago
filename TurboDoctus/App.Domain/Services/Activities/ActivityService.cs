﻿using App.Common.Security;
using App.Domain.Base;
using App.Domain.DTO.Activities;
using App.Infrastructure.Model.Model;
using App.Infrastructure.Repository.Core;
using App.Infrastructure.Repository.Core.Activities;
using AutoMapper;
using doct.Cache;
using doctFramework.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.Services.Activities
{
    public class ActivityService : BaseService<ActivityDTO, Activity>, IActivityService
    {

        public ActivityService(IUnitOfWork unitOfWork, ICacheStorage cacheStorage, ILog log, IUserSession userSession, IActivityRepository repository)
            : base (unitOfWork, repository, cacheStorage, log, userSession)
        {

        }

        public ActivityService(IUnitOfWork unitOfWork, ICacheStorage cacheStorage, IActivityRepository repository)
            : base(unitOfWork, repository, cacheStorage)
        {

        }

        public void CreateActivity(ActivityDTO DTO)
        {
            Activity entiti = Mapper.Map<Activity>(DTO);
            _repository.Insert(entiti);
        }

        public void UpdateActivity(ActivityDTO DTO)
        {
            Activity entiti = Mapper.Map<Activity>(DTO);
            //_repository.Update(entiti);
        }

        public void DeleteActivity(ActivityDTO DTO)
        {
            Activity entiti = Mapper.Map<Activity>(DTO);
            _repository.Delete(entiti);
        }

        public void searchActivity(ActivityDTO DTO)
        {
            Activity entiti = Mapper.Map<Activity>(DTO);
            //_repository.All(entiti);
        }



    }
}
