﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.DTO.Employees
{
    public class EmployeeDTO
    {

        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeIdentity { get; set; }
        public string EmployeeUser { get; set; }
        public string Employeepassword { get; set; }
        public DateTime EmployeeDateCreate { get; set; }
        
    }
}
