﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.DTO.Users
{
	public class ClaimDTO
	{
		public int ClaimId { get; set; }
		public System.Guid UserId { get; set; }
		public string ClaimType { get; set; }
		public string ClaimValue { get; set; }

	}
}
