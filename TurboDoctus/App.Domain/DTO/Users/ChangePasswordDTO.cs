﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.DTO.Users
{
	public class ChangePasswordDTO
	{
		[UIHint("Hidden")]
		[Editable(false)]
		public int UserId { get; set; }
		[UIHint("Hidden")]
		[Editable(false)]
		public int Id { get; set; }
		public string UserName { get; set; }
		[DataType(DataType.Password)]
		public string OldPassword { get; set; }
		[DataType(DataType.Password)]
		public string NewPassword { get; set; }
		[DataType(DataType.Password)]
		public string ConfirmPassword { get; set; }
	}
}
