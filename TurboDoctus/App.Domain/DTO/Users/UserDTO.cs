﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.DTO.Users
{
	public class UserDTO
	{
		[UIHint("Hidden")]
		[Editable(false)]
		public int UserId { get; set; }
		[UIHint("Hidden")]
		[Editable(false)]
		public int Id { get; set; }
		public string UserName { get; set; }
		[DataType(DataType.Password)]
		public string Password { get; set; }
		[DataType(DataType.Password)]
		[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
		public string ConfirmPassword { get; set; }
		public string FullName { get; set; }
		public bool IsBlocked { get; set; }
		public List<RoleDTO> Roles { get; set; }
	}
}
