﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.DTO.WorkingHoures
{
    public class WorkingHourDTO
    {
        public int WorkingHourId { get; set; }
        public int ActivityId { get; set; }
        public int WorkingHourNum { get; set; }
        public DateTime WorkingHourDate { get; set; }
        public DateTime WorkingHourDateCreate { get; set; }
        
    }
}
