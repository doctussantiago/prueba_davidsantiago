﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.DTO.Activities
{
    public class ActivityDTO
    {
        public int ActivityId { get; set; }
        public string ActivityDescription { get; set; }
        public int EmployeeId { get; set; }
        public DateTime ActivityDateCreate { get; set; }
    }
}
