﻿
using App.Domain.DTO.Users;
using App.Infrastructure.Model.Model;
using App.Domain.DTO.Users;
using AutoMapper;
using App.Infrastructure.Model.SecurityEntities;

namespace App.Domain.Config
{
    public class AutomapperConfig
	{
        public static void CreateMaps()
    	{
            Mapper.CreateMap<IdentityUser, UserDTO>().ReverseMap();
			Mapper.CreateMap<IdentityRole, RoleDTO>().ReverseMap();
			Mapper.CreateMap<Claim, System.Security.Claims.Claim>().ReverseMap();
            
    		
        }
    }
}