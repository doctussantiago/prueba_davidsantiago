﻿using Autofac;
using Autofac.Core;
using doct.Cache;
using Doct.ASPNETCacheStorage;
using Doct.EFLogStorage;
using App.Domain.Services.Users;
using Microsoft.AspNet.Identity;
using App.Domain.DTO.Users;
using System;

namespace App.Domain.Config
{
	public class AutofacConfig
	{
		public static void RegisterTypes(ContainerBuilder builder)
		{
			App.Infrastructure.Repository.Config.AutofacConfig.RegisterTypes(builder);
			builder.RegisterAssemblyTypes(typeof(CacheStorage).Assembly)
				.Where(t => t.Name.EndsWith("CacheStorage"))
				.AsImplementedInterfaces().InstancePerRequest();

			builder.RegisterAssemblyTypes(typeof(EFLogStorage).Assembly)
				.Where(t => t.Name.EndsWith("LogStorage"))
				.AsImplementedInterfaces().InstancePerRequest();

			builder.RegisterAssemblyTypes(typeof(UserService).Assembly)
				.Where(t => t.Name.EndsWith("Service"))
				.AsImplementedInterfaces().InstancePerRequest();

		}
	}
}
